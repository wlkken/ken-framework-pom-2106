package com.qf.ability.gateway.security.ignore;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "auth.ignore")
public class AuthIgnoreConfig {

    private String[] urls;
}

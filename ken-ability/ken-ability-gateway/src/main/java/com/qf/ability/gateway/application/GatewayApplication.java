package com.qf.ability.gateway.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@SpringBootApplication(scanBasePackages = "com.qf")
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    /**
     * gateway配置跨域
     */
    @Bean
    public CorsWebFilter getCorsWebFilter(){
        System.out.println("跨域过滤器触发！！");
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //允许cookie跨域
        corsConfiguration.setAllowCredentials(true);
        //允许所有客户端来源的请求跨域
        corsConfiguration.addAllowedOrigin("*");
        //允许所有响应头中的数据跨域
        corsConfiguration.addAllowedHeader("*");
        //允许所有类型请求跨域
        corsConfiguration.addAllowedMethod("*");//POST,GET,PUT

        corsConfiguration.setMaxAge(Long.MAX_VALUE);
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsWebFilter(urlBasedCorsConfigurationSource);
    }
}

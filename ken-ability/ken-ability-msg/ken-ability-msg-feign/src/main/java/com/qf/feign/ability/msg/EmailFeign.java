package com.qf.feign.ability.msg;

import com.qf.ability.msg.input.EmailInput;
import com.qf.common.core.base.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "ken-ability-msg", contextId = "email-feign")
public interface EmailFeign {

    @RequestMapping("/open/api/msg/email/send")
    R sendEmail(@RequestBody EmailInput email);
}

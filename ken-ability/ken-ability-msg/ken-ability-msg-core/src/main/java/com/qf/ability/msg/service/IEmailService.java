package com.qf.ability.msg.service;

import com.qf.ability.msg.input.EmailInput;

public interface IEmailService {

    void sendMail(EmailInput emailInput);
}

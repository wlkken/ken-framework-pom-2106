package com.qf.ability.msg.controller;

import com.qf.ability.msg.input.EmailInput;
import com.qf.ability.msg.service.IEmailService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.feign.ability.msg.EmailFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
public class EmailController implements EmailFeign {

    @Autowired
    private IEmailService emailService;

    /**
     * 发送邮件
     * /open/api/msg/email/send
     * @param email 相关参数
     * @return
     */
    @Override
    public R sendEmail(@RequestBody EmailInput email) {
        System.out.println("Msg服务接收到发送邮件的请求:" + email);
        emailService.sendMail(email);
        return RetUtils.createSucc(true);
    }
}

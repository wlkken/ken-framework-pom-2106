package com.qf.ability.msg.msghandler;

import com.qf.ability.msg.input.EmailInput;
import com.qf.ability.msg.service.IEmailService;
import com.qf.common.event.consumer.EventType;
import com.qf.common.event.consumer.IEventHandler;
import org.springframework.beans.factory.annotation.Autowired;

@EventType(type = "send-email", isAsync = true)
public class EmailEventHandler implements IEventHandler<EmailInput> {

    @Autowired
    private IEmailService emailService;

    @Override
    public void eventHandler(EmailInput event) {
        System.out.println(Thread.currentThread().getName() + " 接收到发送邮件的事件：" + event);
        //调用业务层发送邮件
        emailService.sendMail(event);

        System.out.println("邮件发送成功！");
    }
}

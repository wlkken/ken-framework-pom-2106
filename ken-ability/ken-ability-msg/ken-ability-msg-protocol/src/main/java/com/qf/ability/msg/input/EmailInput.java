package com.qf.ability.msg.input;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class EmailInput implements Serializable {

    //目标的邮件地址
    private String toEmail;
    //标题
    private String subject;
    //内容
    private String content;
}

package com.qf.ability.auth.protocol.input;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class SysUserInput implements Serializable {

    private String username;
    private String password;
    private String nickname;
}

package com.qf.ability.auth.protocol.input;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class SysRoleInput implements Serializable {

    private String roleName;
    private String roleFlag;
}

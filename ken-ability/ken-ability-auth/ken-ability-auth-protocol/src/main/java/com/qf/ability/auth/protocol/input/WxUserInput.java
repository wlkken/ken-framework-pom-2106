package com.qf.ability.auth.protocol.input;

import lombok.Data;

@Data
public class WxUserInput {

    //用户名
    private String username;
    //密码
    private String password;
    //昵称
    private String nickName;
    //邮箱地址
    private String email;
    //头像
    private String headerImg;
    //验证码
    private String code;
}

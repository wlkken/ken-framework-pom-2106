package com.qf.ability.auth.exception;

import com.qf.common.core.base.Codes;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AuthExceptionHandler {

    /**
     * 认证失败异常处理
   InternalAuthenticationServiceException - 用户名错误
   InvalidGrantException - 密码错误
    */
    @ExceptionHandler({
            InternalAuthenticationServiceException.class,
            InvalidGrantException.class
    })
    public R authHandler(){
        return RetUtils.create(Codes.AUTH_ERROR, null);
    }

    /**
     * 令牌刷新失败的异常处理
     * @return
     */
    @ExceptionHandler({InvalidTokenException.class})
    public R refreTokenHandler(){
        return RetUtils.create(Codes.TOKEN_REFRESH_ERROR, null);
    }
}

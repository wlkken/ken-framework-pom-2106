package com.qf.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.auth.SysRole;
import com.qf.data.entity.auth.vo.SysRoleVo;

import java.util.List;

/**
 * (SysRole)表服务接口
 *
 * @author makejava
 * @since 2021-10-21 14:26:13
 */
public interface SysRoleService extends IService<SysRole> {

    List<SysRoleVo> queryRolesByUid(Long uid);

    List<SysRole> queryHasRolesByUid(Long uid);

    int updateUserRoles(Long uid, List<Long> rids);

}

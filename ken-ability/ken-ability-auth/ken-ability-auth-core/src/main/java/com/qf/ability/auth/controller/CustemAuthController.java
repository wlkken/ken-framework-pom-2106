package com.qf.ability.auth.controller;

import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

/**
 * 自定义的认证处理器
 */
@RestController
@RequestMapping("/oauth")
public class CustemAuthController {

    @Autowired
    private TokenEndpoint tokenEndpoint;

    /**
     * 自定义的令牌申请接口
     * @return
     */
    @RequestMapping("/token")
    public R autnHandler(Principal principal, @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {

        //手动调用Oauth2生成令牌
        ResponseEntity<OAuth2AccessToken> responseEntity = tokenEndpoint.postAccessToken(principal, parameters);
        OAuth2AccessToken body = responseEntity.getBody();

//        if (principal instanceof WxUserVo) {
//            //手动增强 -
//            WxUserVo wxUser = (WxUserVo) principal;
//            Map<String, Object> additionalInformation = body.getAdditionalInformation();
//            additionalInformation.put("nick_name", wxUser.getNickName());
//            additionalInformation.put("headerImg", wxUser.getHeaderImg());
//        }

        return RetUtils.createSucc(body);
    }
}

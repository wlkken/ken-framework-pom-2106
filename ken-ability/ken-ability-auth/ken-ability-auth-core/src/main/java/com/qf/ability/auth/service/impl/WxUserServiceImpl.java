package com.qf.ability.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.ability.auth.service.WxUserService;
import com.qf.data.entity.auth.WxUser;
import com.qf.data.entity.auth.vo.WxUserVo;
import com.qf.data.mapper.auth.WxUserDao;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (WxUser)表服务实现类
 *
 * @author makejava
 * @since 2021-11-11 09:16:49
 */
@Service("userDetailsService2")
public class WxUserServiceImpl extends ServiceImpl<WxUserDao, WxUser> implements WxUserService {

    @Autowired
    private WxUserDao wxUserDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //根据用户名查询微信用户信息
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        List<WxUser> wxUsers = wxUserDao.selectByMap(map);
        if (CollectionUtils.isEmpty(wxUsers)) return null;

        WxUser wxUser = wxUsers.get(0);
        WxUserVo wxUserVo = new WxUserVo();
        BeanUtils.copyProperties(wxUser, wxUserVo);
        return wxUserVo;
    }
}

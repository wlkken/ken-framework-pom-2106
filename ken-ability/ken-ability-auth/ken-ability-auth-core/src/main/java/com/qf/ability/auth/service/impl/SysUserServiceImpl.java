package com.qf.ability.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.ability.auth.service.SysPowerService;
import com.qf.ability.auth.service.SysRoleService;
import com.qf.ability.auth.service.SysUserService;
import com.qf.data.entity.auth.SysPower;
import com.qf.data.entity.auth.SysRole;
import com.qf.data.entity.auth.SysUser;
import com.qf.data.entity.auth.vo.SysUserVo;
import com.qf.data.mapper.auth.SysUserDao;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * (SysUser)表服务实现类
 *
 * @author makejava
 * @since 2021-10-21 14:25:46
 */
@Service("userDetailsService1")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUser> implements SysUserService {

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysPowerService sysPowerService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        List<SysUser> sysUsers = sysUserDao.selectByMap(map);
        SysUser sysUser = null;
        if (!CollectionUtils.isEmpty(sysUsers)) sysUser = sysUsers.get(0);
        Assert.notNull(sysUser, "用户名或密码错误！");


        //转换成Vo对象
        SysUserVo sysUserVo = new SysUserVo();
        BeanUtils.copyProperties(sysUser, sysUserVo);

        //查询当前用户的权限并且设置
        //根据用户查角色
        List<SysRole> sysRoles = sysRoleService.queryHasRolesByUid(sysUserVo.getUId());

        //获取当前用户拥有的角色id集合
        List<Long> roids = sysRoles
                .stream()
                .map(sysRole -> sysRole.getRoId())
                .collect(Collectors.toList());

        //获得当前用户拥有的权限列表
        List<SysPower> sysPowers = sysPowerService.queryHasPowersByRoid(roids);

        //将角色和权限转换成GrantedAuthority对象
        List<GrantedAuthority> authorityRoleList = sysRoles.stream()
                .map(sysRole -> new SimpleGrantedAuthority("ROLE_" + sysRole.getRoleFlag()))
                .collect(Collectors.toList());

        List<GrantedAuthority> authorityPowerList = sysPowers.stream()
                .filter(sysPower -> sysPower.getPrFlag() != null)//设置的权限路径 绝对不能为null
                .map(sysPower -> new SimpleGrantedAuthority(sysPower.getPrFlag()))
                .collect(Collectors.toList());

        authorityRoleList.addAll(authorityPowerList);
        //设置当前用户拥有的权限/角色
        sysUserVo.setAuthorities(authorityRoleList);
        return sysUserVo;
    }
}

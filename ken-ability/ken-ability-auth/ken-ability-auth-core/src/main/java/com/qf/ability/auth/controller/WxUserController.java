package com.qf.ability.auth.controller;

import com.qf.ability.auth.protocol.input.WxUserInput;
import com.qf.ability.auth.service.WxUserService;
import com.qf.ability.msg.input.EmailInput;
import com.qf.common.core.base.Codes;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.common.event.publish.EventSend;
import com.qf.data.entity.auth.WxUser;
import com.qf.feign.ability.msg.EmailFeign;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户端的Controller
 */
@RestController
@RequestMapping("/open/api/user")
public class WxUserController {


    @Autowired
    private EmailFeign emailFeign;

    @Autowired
    private WxUserService wxUserService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EventSend eventSend;

    /**
     * ??? HashMap/ArrayList/LinkedList - 线程不安全
     * 环境一定是多线程环境？？？
     *
     * 如果不解决，会造成什么后果？
     *
     * Map<String, List<Object>>
     *     email - [code,  Thread.currentThread() + 300000]
     *
     */
    private Map<String, String> map = new ConcurrentHashMap<>();

    /**
     * 基于邮箱发送验证码
     * @return
     */
    @RequestMapping("/email/sendCode")
    public R sendCode(String email){

        //生成验证码
        String code = (int)(Math.random() * 90000) + 10000 + "";
        System.out.println("系统验证码：" + code);

        map.put(email, code);

        EmailInput emailInput = new EmailInput();
        emailInput.setToEmail(email);//收件人
        emailInput.setSubject("暴雪官方验证码");//标题
        emailInput.setContent("<h1>公司的Logo</h1>当验证码为：" + code); //10000 ~ 99999

        //发送邮件
//        emailFeign.sendEmail(emailInput);

//        //发送消息到RabbitMQ
//        rabbitTemplate.convertAndSend("msg-exchange", "", emailInput);

        //调用事件总线，发布邮件发送的事件
        eventSend.send("send-email", emailInput);

        return RetUtils.createSucc(true);
    }

    /**
     * 注册前端用户
     * @return
     */
    @RequestMapping("/register")
    public R register(WxUserInput wxUserInput){

        System.out.println("当前controller对象：" + this.hashCode() + " 线程名称：" + Thread.currentThread().getName());

        //校验验证码
        String code = wxUserInput.getCode();
        String email = wxUserInput.getEmail();

        synchronized (email.intern()) {
            String code1 = map.get(email);//线程1 - email    //线程2 - email
            //验证码对比
            if (code != null && !code.equals(code1))
                return RetUtils.create(Codes.CODE_ERROR, null);
            //验证正确
            map.remove(email);
        }

        //保存用户
        WxUser wxUser = new WxUser();
        BeanUtils.copyProperties(wxUserInput, wxUser);
        String password = passwordEncoder.encode(wxUser.getPassword());
        System.out.println("加密后的密码：" + password);
        wxUser.setPassword(password);//密码加密
        wxUserService.save(wxUser);

        return RetUtils.createSucc(true);
    }
}

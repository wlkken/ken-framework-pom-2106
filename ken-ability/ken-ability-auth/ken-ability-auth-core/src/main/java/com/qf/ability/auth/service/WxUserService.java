package com.qf.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.auth.WxUser;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * (WxUser)表服务接口
 *
 * @author makejava
 * @since 2021-11-11 09:16:49
 */
public interface WxUserService extends IService<WxUser>, UserDetailsService {

}

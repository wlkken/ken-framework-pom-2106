package com.qf.ability.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.ability.auth.service.SysPowerService;
import com.qf.data.entity.auth.SysPower;
import com.qf.data.entity.auth.SysRolePower;
import com.qf.data.entity.auth.dto.SysPowerDto;
import com.qf.data.entity.auth.vo.SysPowerVo;
import com.qf.data.mapper.auth.SysPowerDao;
import com.qf.data.mapper.auth.SysRolePowerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * (SysPower)表服务实现类
 *
 * @author makejava
 * @since 2021-10-22 09:29:28
 */
@Service
public class SysPowerServiceImpl extends ServiceImpl<SysPowerDao, SysPower> implements SysPowerService {

    @Autowired
    private SysPowerDao sysPowerDao;

    @Autowired
    private SysRolePowerDao sysRolePowerDao;

    @Override
    public List<SysPowerVo> queryAll() {
        return sysPowerDao.queryAll();
    }

    @Override
    public List<SysPowerDto> queryPowersByRoid(Long roid) {
        return sysPowerDao.queryPowersByRoid(roid);
    }

    @Override
    public List<SysPower> queryPowersByUid(Long uid) {
        return sysPowerDao.queryHasPowersByUid(uid);
    }

    /**
     * 根据角色id集合，查询这些角色拥有的权限列表
     * @param roids
     * @return
     */
    @Override
    public List<SysPower> queryHasPowersByRoid(List<Long> roids) {
        return sysPowerDao.queryHasPowersByRoid(roids);
    }

    /**
     * 修改觉得对应的权限id列表
     * @param roid
     * @param poids
     * @return
     */
    @Override
    @Transactional
    public int updatePowerByRole(Long roid, List<Long> poids) {

        //根据角色id删除所有的关联关系
        UpdateWrapper<SysRolePower> wrapper = new UpdateWrapper<>(new SysRolePower()).eq("ro_id", roid);
        sysRolePowerDao.delete(wrapper);

        //循环添加新的关联关系（角色 - 权限）
        poids.stream()
                .map(poid -> new SysRolePower().setRoId(roid).setPoId(poid).setCreateTime(new Date()))
                .forEach(sysRolePower -> {
                    sysRolePowerDao.insert(sysRolePower);
                });
        return 1;
    }
}

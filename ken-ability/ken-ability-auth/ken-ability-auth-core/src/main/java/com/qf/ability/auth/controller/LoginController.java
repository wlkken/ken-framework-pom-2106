package com.qf.ability.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    /**
     * 身份认证管理器 - 进行身份认证（判断当前用户是否存在）
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping("/login")
    public void login(String username, String password){

        //代表当前的用户身份 （未通过身份认证 已通过身份认证）
        Authentication authentication = null;

            authentication = new UsernamePasswordAuthenticationToken(username, password);
        //进行身份认证 - 通过UserDetailsService接口进行用户自定义的认证过程
        //如果身份认证通过 -
        // 返回的对象的 isAuthenticated() 会返回true
        // principal属性 就不在是用户名了，而变成了UserDetils对象
        // getAuthorities()方法就会返回当前用户的权限(角色)
        authentication = authenticationManager.authenticate(authentication);

        //放入Security容器中，便于后续访问
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //Oauth2 -- 通过authentication生成access_token
    }
}

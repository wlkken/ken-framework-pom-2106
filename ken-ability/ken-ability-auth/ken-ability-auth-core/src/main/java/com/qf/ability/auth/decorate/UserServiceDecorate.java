package com.qf.ability.auth.decorate;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserServiceDecorate extends UserDetailsService {
}

package com.qf.ability.auth.controller;

import com.qf.ability.auth.protocol.input.SysUserInput;
import com.qf.ability.auth.service.SysUserService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.data.entity.auth.SysUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/sys/user")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 查询系统用户
     * @return
     */
    @PreAuthorize("hasAuthority('/SysUserManager')")
    @RequestMapping("/queryAll")
    public R queryAll(){

        List<SysUser> sysUsers = sysUserService.list()
                .stream()
                .map(sysUser -> sysUser.setPassword(null))
                .collect(Collectors.toList());

        return RetUtils.createSucc(sysUsers);
    }

    /**
     * 新增系统用户
     * @return
     */
    @RequestMapping("/insert")
    public R insert(@Valid SysUserInput input){
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(input, sysUser);
        sysUserService.save(sysUser);
        return RetUtils.createSucc(true);
    }
}

package com.qf.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.auth.SysUser;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * (SysUser)表服务接口
 *
 * @author makejava
 * @since 2021-10-21 14:25:46
 */
public interface SysUserService extends IService<SysUser>, UserDetailsService {

}

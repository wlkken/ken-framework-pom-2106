package com.qf.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.auth.SysPower;
import com.qf.data.entity.auth.dto.SysPowerDto;
import com.qf.data.entity.auth.vo.SysPowerVo;

import java.util.List;

/**
 * (SysPower)表服务接口
 *
 * @author makejava
 * @since 2021-10-22 09:29:28
 */
public interface SysPowerService extends IService<SysPower> {

    List<SysPowerVo> queryAll();

    List<SysPowerDto> queryPowersByRoid(Long roid);

    List<SysPower> queryPowersByUid(Long uid);

    List<SysPower> queryHasPowersByRoid(List<Long> roids);

    int updatePowerByRole(Long roid, List<Long> poids);
}

package com.qf.ability.auth.application;

import com.qf.common.core.sentinel.EnableSentinelConfig;
import com.qf.common.event.annotation.EnableEventPublish;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableSentinelConfig
@EnableEventPublish
public class AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }

    /**
     * 配置密码处理器
     * @return
     */
    @Bean
    public PasswordEncoder getPasswordEncoder(){
//        return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder();
    }

//    /**
//     * 配置跨域的过滤器
//     * @return
//     */
//    @Bean
//    public CorsFilter getCorsFilter(){
//        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
//        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        //允许cookie跨域
//        corsConfiguration.setAllowCredentials(true);
//        //允许所有客户端来源的请求跨域
//        corsConfiguration.addAllowedOrigin("*");
//        //允许所有响应头中的数据跨域
//        corsConfiguration.addAllowedHeader("*");
//        //允许所有类型请求跨域
//        corsConfiguration.addAllowedMethod("*");//POST,GET,PUT
//        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
//        CorsFilter corsFilter = new CorsFilter(urlBasedCorsConfigurationSource);
//        return corsFilter;
//    }
//
//    /**
//     * 注册指定的过滤器
//     * @return
//     */
//    @Bean
//    public FilterRegistrationBean getFilterRegistrationBean(CorsFilter getCorsFilter){
//        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
//        filterRegistrationBean.setName("CorsFilter");
//        filterRegistrationBean.setFilter(getCorsFilter);
//        filterRegistrationBean.addUrlPatterns("/*");
//        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
//        return filterRegistrationBean;
//    }
}

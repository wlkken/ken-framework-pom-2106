package com.qf.business.hotel.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.hotel.service.CPriceService;
import com.qf.business.hotel.service.CRoomService;
import com.qf.common.core.utils.DateUtils;
import com.qf.common.event.publish.EventSend;
import com.qf.data.entity.hotel.CPrice;
import com.qf.data.entity.hotel.CRoom;
import com.qf.data.entity.hotel.vo.CRoomVo;
import com.qf.data.mapper.hotel.CRoomDao;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * (CRoom)表服务实现类
 *
 * @author makejava
 * @since 2021-11-13 13:02:37
 */
@Service
public class CRoomServiceImpl extends ServiceImpl<CRoomDao, CRoom> implements CRoomService {

    @Autowired
    private CPriceService cPriceService;

    @Autowired
    private EventSend eventSend;

    @Autowired
    private CRoomDao cRoomDao;


    /**
     * 保存客房信息
     * @param entity
     * @return
     */
    @Override
    @Transactional
    public boolean save(CRoom entity) {
        boolean flag = super.save(entity);

        List<CPrice> cPrices = new ArrayList<>();
        //生成30天的价格信息
        for (int i = 0; i < 30; i++) {
            CPrice cPrice = new CPrice()
                    .setRId(entity.getRId())//设置客房id
                    .setPDate(DateUtils.getNowDate(i))//设置日期
                    .setPPrice(entity.getRoomDefaultPrice())
                    .setPHasNumber(entity.getRoomNumber());

            cPriceService.save(cPrice);
            cPrices.add(cPrice.setHid(entity.getHId()));
        }

        //发送客房新增事件
        eventSend.send("room-insert", entity);
        //将价格信息通过事件总线发送到搜索服务
        System.out.println("发送价格列表：" + cPrices);
        eventSend.send("price-insert", cPrices);
        return flag;
    }

    /**
     * 根据酒店ID 和  入住时间离店时间 查询客房列表
     * @param hid
     * @param beginTime
     * @param endTime
     * @return
     */
    @Override
    public List<CRoomVo> queryRoomListByHid(Long hid, Date beginTime, Date endTime) {

        //查询房间列表
        List<CRoom> rooms = super.query()
                .eq("h_id", hid)
                .list();

        //对象的转换
        return rooms.stream().map(cRoom -> {
            //查询固定时间范围内的价格信息
            List<CPrice> prices = cPriceService.query()
                    .eq("r_id", cRoom.getRId())
                    .ge("p_date", beginTime)
                    .lt("p_date", endTime).list();

            //总价
            BigDecimal allPrice = BigDecimal.ZERO;
            //平均价
            BigDecimal avgPrice = BigDecimal.ZERO;
            //天数
            int days = 0;

            //计算平均价格
            if(!CollectionUtils.isEmpty(prices)){
                for (CPrice price : prices) {
                    //获取房间的价格
                    allPrice = allPrice.add(price.getPPrice());
                    //天数累加
                    days++;
                }

                //计算平均价
                avgPrice = allPrice.divide(BigDecimal.valueOf(days)).setScale(2);
            }

            //转换
            CRoomVo cRoomVo = new CRoomVo();
            BeanUtils.copyProperties(cRoom, cRoomVo);
            cRoomVo.setRoomAvgPrice(avgPrice);
            return cRoomVo;
        }).collect(Collectors.toList());
    }
}

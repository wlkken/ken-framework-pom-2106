package com.qf.business.hotel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.hotel.CRoom;
import com.qf.data.entity.hotel.vo.CRoomVo;

import java.util.Date;
import java.util.List;

/**
 * (CRoom)表服务接口
 *
 * @author makejava
 * @since 2021-11-13 13:02:37
 */
public interface CRoomService extends IService<CRoom> {

    List<CRoomVo> queryRoomListByHid(Long hid, Date beginTime, Date endTime);
}

package com.qf.business.hotel.controller;

import com.qf.business.hotel.protocol.input.CRoomInput;
import com.qf.business.hotel.service.CRoomService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.data.entity.hotel.CRoom;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private CRoomService cRoomService;

    /**
     * 查询多个客房
     * @return
     */
    @RequestMapping("/queryAll")
    public R queryAll(){
        List<CRoom> rooms = cRoomService.list();
        return RetUtils.createSucc(rooms);
    }

    /**
     * 新增客房
     * @param roomInput
     * @return
     */
    @RequestMapping("/insert")
    public R insertRoom(CRoomInput roomInput){

        CRoom cRoom = new CRoom();
        BeanUtils.copyProperties(roomInput, cRoom);

        boolean falg = cRoomService.save(cRoom);
        return RetUtils.createSucc(falg);
    }
}

package com.qf.business.hotel.appliation;

import com.qf.common.redis.utils.BloomUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InitApplication implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        BloomUtils.initBloom("hotel-djl-bloom", 0.001, 10000000L);
    }
}

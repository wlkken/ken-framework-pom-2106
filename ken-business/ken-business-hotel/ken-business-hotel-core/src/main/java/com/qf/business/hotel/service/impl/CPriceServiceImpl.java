package com.qf.business.hotel.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.hotel.service.CPriceService;
import com.qf.business.hotel.service.CRoomService;
import com.qf.common.event.publish.EventSend;
import com.qf.data.entity.hotel.CPrice;
import com.qf.data.entity.hotel.CRoom;
import com.qf.data.mapper.hotel.CPriceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * (CPrice)表服务实现类
 *
 * @author makejava
 * @since 2021-11-13 13:03:04
 */
@Service
public class CPriceServiceImpl extends ServiceImpl<CPriceDao, CPrice> implements CPriceService {

    @Autowired
    private EventSend eventSend;

    @Autowired
    private CRoomService cRoomService;

    @Override
    @Transactional
    public boolean updateById(CPrice entity) {
        super.updateById(entity);

        //根据pid 查询房间id -> 根据客房id 查询客房信息 -> 酒店id
        entity = super.getById(entity.getPId());
        CRoom room = cRoomService.getById(entity.getRId());
        entity.setHid(room.getHId());

        //发送消息
        eventSend.send("update-price", entity);

        return true;
    }
}

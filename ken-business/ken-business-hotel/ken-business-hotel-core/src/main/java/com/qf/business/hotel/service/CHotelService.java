package com.qf.business.hotel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.hotel.CHotel;
import com.qf.data.entity.hotel.vo.CHotelVo;

import java.util.List;

/**
 * (CHotel)表服务接口
 *
 * @author makejava
 * @since 2021-10-20 15:27:44
 */
public interface CHotelService extends IService<CHotel> {

    /**
     * 查询所有酒店以及城市信息
     * @return
     */
    List<CHotelVo> queryAllHotelAndCity();
}

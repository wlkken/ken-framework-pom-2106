package com.qf.business.hotel.controller;

import com.qf.business.hotel.protocol.input.CityInput;
import com.qf.business.hotel.service.CCityService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.common.core.utils.UserInfo;
import com.qf.data.entity.auth.vo.SysUserVo;
import com.qf.data.entity.hotel.CCity;
import com.qf.data.entity.hotel.vo.CityVo;
import com.qf.feign.business.hotel.CityFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class CityController implements CityFeign {

    @Autowired
    private CCityService cCityService;

    //Authentication对象
    @Override
    @PreAuthorize("hasAuthority('/CityManager')")
    public R<List<CCity>> queryAll(){
        log.info("[city-queryall] 查询所有的城市信息....");

        //获取当前的登录用户
        SysUserVo user = UserInfo.getUser();
        System.out.println("当前登录的用户信息：" + user);

        List<CCity> cityList = cCityService.list();
        return RetUtils.createSucc(cityList);
    }

    @PreAuthorize("hasAuthority('/CityInsert')")
    @Override
    public R insert(CityInput cityInput){
        log.info("[city-insert] 添加城市信息 - {}", cityInput);
        CCity cCity = new CCity();
        BeanUtils.copyProperties(cityInput, cCity);
        boolean falg = cCityService.save(cCity);
        return RetUtils.createSucc(falg);
    }

    /**
     * 根据城市Id 查询城市实体对象
     * @param cid
     * @return
     */
    @Override
    public R<CCity> queryById(Long cid) {
        CCity cCity = cCityService.getById(cid);
        return RetUtils.createSucc(cCity);
    }

    /**
     * 前端获取城市列表 （拼音 - 首字母）
     * @return
     */
    @RequestMapping("/open/api/city/list")
    public R frontCityList(){
        System.out.println("Controller的线程：" + Thread.currentThread().getName());
        List<CityVo> cityVos = cCityService.frontQueryAll();
        return RetUtils.createSucc(cityVos);
    }
}

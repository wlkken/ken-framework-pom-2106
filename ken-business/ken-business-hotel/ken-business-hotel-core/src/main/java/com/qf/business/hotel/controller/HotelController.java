package com.qf.business.hotel.controller;

import com.qf.business.hotel.protocol.input.CHotelInput;
import com.qf.business.hotel.protocol.input.OrderPriceInput;
import com.qf.business.hotel.protocol.output.OrderPriceOutput;
import com.qf.business.hotel.service.CHotelService;
import com.qf.business.hotel.service.CRoomService;
import com.qf.business.hotel.service.IOrderService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.common.core.utils.UserInfo;
import com.qf.common.redis.utils.BloomUtils;
import com.qf.data.entity.auth.vo.WxUserVo;
import com.qf.data.entity.hotel.CHotel;
import com.qf.data.entity.hotel.vo.CHotelVo;
import com.qf.data.entity.hotel.vo.CRoomVo;
import com.qf.feign.business.hotel.HotelFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@Slf4j
public class HotelController implements HotelFeign {

    @Autowired
    private CHotelService hotelService;

    @Autowired
    private CRoomService roomService;

    @Autowired
    private IOrderService orderService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 查询所有酒店
     * @return
     */
    @Override
    @PreAuthorize("hasAuthority('/HotelManager')")
    public R queryAll(){
        log.info("[hotel-queryall] 查询所有酒店列表....");
        List<CHotelVo> cHotels = hotelService.queryAllHotelAndCity();
        return RetUtils.createSucc(cHotels);
    }

    @Override
    @PreAuthorize("hasAuthority('/HotelInsert')")
    public R insert(@Valid CHotelInput hotelInput){
        log.info("[hotel-insert] 添加酒店信息 - {}", hotelInput);
        CHotel cHotel = new CHotel();
        BeanUtils.copyProperties(hotelInput, cHotel);
        boolean falg = hotelService.save(cHotel);
        return RetUtils.createSucc(falg);
    }

    /**
     * 查询酒店详情
     * @return
     */
    @RequestMapping("/open/api/hotel/details")
    public R<CHotel> queryDetails(Long hid){

        //统计酒店的点击量
        //获取用户信息
        WxUserVo user = UserInfo.getUser();
        if (user != null) {
            //当前请求已经登录，进行点击量的统计
            String uhids = user.getWuId() + "-" + hid;
            //判断布隆过滤器
            if(BloomUtils.addBloom("hotel-djl-bloom", uhids)){
                //如果不存在，则计算一次点击量
                if (stringRedisTemplate.opsForHash().hasKey("hotel-djl", hid + "")) {
                    //已经存在该酒店id
                    stringRedisTemplate.opsForHash().increment("hotel-djl", hid + "", 1);
                } else {
                    //如果不存在则初始化一个点击量
                    stringRedisTemplate.opsForHash().put("hotel-djl", hid + "", "1");
                }
            }
        }


        CHotel cHotel = hotelService.getById(hid);
        return RetUtils.createSucc(cHotel);
    }


    /**
     * 请求酒店的客房列表
     * @return
     */
    @RequestMapping("/open/api/rooms/list")
    public R roomsList(Long hid,
                       @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginTime,
                       @DateTimeFormat(pattern = "yyyy-MM-dd") Date endTime) {
        List<CRoomVo> cRoomVos = roomService.queryRoomListByHid(hid, beginTime, endTime);
        System.out.println("查询的结果：" + cRoomVos);
        return RetUtils.createSucc(cRoomVos);
    }


    /**
     * 后端Feign调用的方法
     * @return
     */
    @RequestMapping("/order/prices")
    public R<OrderPriceOutput> getOrderPricesFeign(@RequestBody OrderPriceInput orderPriceInput){
        //调用业务层，获取当前订单的价格明细
        OrderPriceOutput orderPrice = orderService.getOrderPrice(orderPriceInput);
        return RetUtils.createSucc(orderPrice);
    }

    /**
     * 获取订单的价格明细
     * @return
     */
    @RequestMapping("/open/api/order/prices")
    public R<OrderPriceOutput> getOrderPrices(OrderPriceInput orderPriceInput){
        //调用业务层，获取当前订单的价格明细
        OrderPriceOutput orderPrice = orderService.getOrderPrice(orderPriceInput);
        return RetUtils.createSucc(orderPrice);
    }
}

package com.qf.business.hotel.controller;

import com.qf.business.hotel.protocol.input.CPriceInput;
import com.qf.business.hotel.service.CPriceService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.data.entity.hotel.CPrice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/room/price")
public class RoomPriceController {

    @Autowired
    private CPriceService cPriceService;

    /**
     * 根据客房id查询价格列表
     * @param rid
     * @return
     */
    @RequestMapping("/queryByRid")
    public R queryPriceByRid(Long rid){
        List<CPrice> cPrices = cPriceService.query().eq("r_id", rid).list();
        return RetUtils.createSucc(cPrices);
    }

    /**
     * 修改客房价格
     * @return
     */
    @RequestMapping("/updatePrice")
    public R updatePrice(CPriceInput cPriceInput){

        CPrice cPrice = new CPrice();
        BeanUtils.copyProperties(cPriceInput, cPrice);
        System.out.println("修改的cPrice：" + cPrice);
        cPriceService.updateById(cPrice);

        return RetUtils.createSucc(true);
    }
}

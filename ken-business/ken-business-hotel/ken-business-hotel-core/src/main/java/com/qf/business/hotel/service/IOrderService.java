package com.qf.business.hotel.service;

import com.qf.business.hotel.protocol.input.OrderPriceInput;
import com.qf.business.hotel.protocol.output.OrderPriceOutput;

public interface IOrderService {

    OrderPriceOutput getOrderPrice(OrderPriceInput orderPriceInput);
}

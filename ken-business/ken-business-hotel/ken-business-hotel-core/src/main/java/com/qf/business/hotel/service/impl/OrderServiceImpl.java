package com.qf.business.hotel.service.impl;

import com.qf.business.coupon.rule.ILimit;
import com.qf.business.coupon.rule.IRule;
import com.qf.business.coupon.utils.CouponUtils;
import com.qf.business.hotel.protocol.input.OrderPriceInput;
import com.qf.business.hotel.protocol.output.OrderPriceOutput;
import com.qf.business.hotel.service.CPriceService;
import com.qf.business.hotel.service.IOrderService;
import com.qf.common.core.base.R;
import com.qf.data.entity.coupon.Coupon;
import com.qf.data.entity.hotel.CPrice;
import com.qf.feign.business.coupon.CouponFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private CPriceService priceService;

    @Autowired
    private CouponFeign couponFeign;

    @Override
    public OrderPriceOutput getOrderPrice(OrderPriceInput orderPriceInput) {

        //总价格
        BigDecimal allPrice = BigDecimal.ZERO;

        //明细列表
        //1 X 1999.00
        List<String> priceDetails = new ArrayList<>();

        List<CPrice> prices = priceService.query()
                .eq("r_id", orderPriceInput.getRid())
                .ge("p_date", orderPriceInput.getBeginTime())
                .lt("p_date", orderPriceInput.getEndTime())
                .list();

        //循环每天的价格信息
        for (CPrice price : prices) {
            //今天的明细
            String dayPrice = orderPriceInput.getNumbers() + " X " + price.getPPrice().setScale(2);
            priceDetails.add(dayPrice);

            //累加总价
            allPrice = allPrice.add(price.getPPrice().multiply(BigDecimal.valueOf(orderPriceInput.getNumbers())).setScale(2));
        }

        //处理优惠券
        if (orderPriceInput.getCpid() != null) {
            //选择了优惠券
            Coupon coupon = null;
            R<Coupon> result = couponFeign.queryCouponById(orderPriceInput.getCpid());
            if (result != null && result.getCode() == 200) {
                coupon = result.getData();
            }
            Assert.notNull(coupon, "优惠券信息异常！");

            //处理优惠券的优惠逻辑
            ILimit limit = CouponUtils.parseCouponGetLimit(coupon);
            IRule rule = CouponUtils.parseCouponGetRule(coupon);

            //判断优惠券是否可以使用
            if (limit.limit(allPrice)){
                //优惠券可用
                BigDecimal youhui = rule.youhui(allPrice);

                //生成一条优惠记录
                String dayRule = " - " + allPrice.subtract(youhui);
                priceDetails.add(dayRule);

                //使用优惠后的价格 覆盖 总价格
                allPrice = youhui;
            }
        }

        OrderPriceOutput orderPriceOutput = new OrderPriceOutput();
        orderPriceOutput.setAllprice(allPrice);
        orderPriceOutput.setPriceDetails(priceDetails);

        //返回结果
        return orderPriceOutput;
    }
}

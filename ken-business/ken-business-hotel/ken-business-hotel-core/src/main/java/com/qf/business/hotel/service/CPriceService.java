package com.qf.business.hotel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.hotel.CPrice;

/**
 * (CPrice)表服务接口
 *
 * @author makejava
 * @since 2021-11-13 13:03:04
 */
public interface CPriceService extends IService<CPrice> {

}

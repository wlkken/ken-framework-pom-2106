package com.qf.business.hotel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.hotel.CCity;
import com.qf.data.entity.hotel.vo.CityVo;

import java.util.List;

/**
 * (CCity)表服务接口
 *
 * @author makejava
 * @since 2021-10-19 15:56:03
 */
public interface CCityService extends IService<CCity> {

    List<CityVo> frontQueryAll();
}

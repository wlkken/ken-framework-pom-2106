package com.qf.business.hotel.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.hotel.service.CHotelService;
import com.qf.common.event.publish.EventSend;
import com.qf.common.redis.cache.annotation.CacheGet;
import com.qf.data.entity.hotel.CHotel;
import com.qf.data.entity.hotel.vo.CHotelVo;
import com.qf.data.mapper.hotel.CHotelDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * (CHotel)表服务实现类
 *
 * @author makejava
 * @since 2021-10-20 15:27:44
 */
@Service
public class CHotelServiceImpl extends ServiceImpl<CHotelDao, CHotel> implements CHotelService {

    @Autowired
    private CHotelDao hotelDao;

    @Autowired
    private EventSend eventSend;

    @Override
    public List<CHotelVo> queryAllHotelAndCity() {
        return hotelDao.queryAllHotelAndCity();
    }

    /**
     * 保存酒店
     * @param entity
     * @return
     */
    @Override
    public boolean save(CHotel entity) {
        //保存酒店信息到数据库
        super.save(entity);

        //将酒店信息同步到es
        eventSend.send("hotel-insert", entity);
        return true;
    }

    @Override
    @CacheGet(cacheName = "hotel", key = "'hid-' + #id")
    public CHotel getById(Serializable id) {
        return super.getById(id);
    }
}

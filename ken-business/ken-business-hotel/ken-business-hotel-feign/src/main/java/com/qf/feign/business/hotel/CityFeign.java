package com.qf.feign.business.hotel;

import com.qf.business.hotel.protocol.input.CityInput;
import com.qf.common.core.base.R;
import com.qf.data.entity.hotel.CCity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@FeignClient(value = "ken-business-hotel", contextId = "city")
public interface CityFeign {

    @RequestMapping("/city/queryAll")
    R<List<CCity>> queryAll();

    @RequestMapping("/city/insert")
    R insert(@Valid CityInput cityInput);

    @RequestMapping("/city/queryById")
    R<CCity> queryById(@RequestParam("cid") Long cid);
}

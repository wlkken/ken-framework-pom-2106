package com.qf.feign.business.hotel;

import com.qf.business.hotel.protocol.input.CHotelInput;
import com.qf.business.hotel.protocol.input.OrderPriceInput;
import com.qf.business.hotel.protocol.output.OrderPriceOutput;
import com.qf.common.core.base.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@FeignClient(value = "ken-business-hotel", contextId = "hotel")
public interface HotelFeign {

    /**
     * 查询所有酒店
     * @return
     */
    @RequestMapping("/hotel/queryAll")
    R queryAll();

    @RequestMapping("/hotel/insert")
    R insert(@Valid CHotelInput hotelInput);

    @RequestMapping("/order/prices")
    R<OrderPriceOutput> getOrderPricesFeign(@RequestBody OrderPriceInput orderPriceInput);
}

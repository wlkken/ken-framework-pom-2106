package com.qf.business.hotel.protocol.input;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CRoomInput implements Serializable {

    private Long hId;
    private String roomName;
    private String roomImages;
    private String roomContent;
    private Integer roomNumber;
    private BigDecimal roomDefaultPrice;
}

package com.qf.business.hotel.protocol.input;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class CityInput implements Serializable {

    /**
     * @NotNull 只校验参数是否为null
     * @NotEmpty 判断空和空字符串（集合长度不能为0）
     * @NotBlank 判断空和空字符串、一堆空格
     */
    @NotBlank(message = "城市名称不能为空")
    private String cityName;
    private String cityPinyin;
}

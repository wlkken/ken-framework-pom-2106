package com.qf.business.hotel.protocol.input;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class CHotelInput implements Serializable {

    @NotBlank(message = "酒店名称不能为空！")
    private String hotelName;
    private Long cId;
    private String hotelImages;
    private Double hotelLon;
    private Double hotelLat;
    private String hotelAddress;
    private Integer hotelLevel;
    private String hotelKeyword;
    private String hotelRegx;
    private String hotelContent;

}

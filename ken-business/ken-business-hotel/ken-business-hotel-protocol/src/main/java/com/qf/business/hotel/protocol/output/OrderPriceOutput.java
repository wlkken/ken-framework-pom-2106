package com.qf.business.hotel.protocol.output;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderPriceOutput implements Serializable {

    //总价格
    private BigDecimal allprice;

    //价格明细列表
    private List<String> priceDetails;
}

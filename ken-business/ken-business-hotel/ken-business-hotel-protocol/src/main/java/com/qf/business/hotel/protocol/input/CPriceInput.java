package com.qf.business.hotel.protocol.input;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CPriceInput implements Serializable {

    private Long pId;
    private BigDecimal pPrice;
    private Integer pHasNumber;
}

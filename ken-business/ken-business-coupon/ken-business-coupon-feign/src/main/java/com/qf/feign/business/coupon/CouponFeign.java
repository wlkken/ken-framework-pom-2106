package com.qf.feign.business.coupon;

import com.qf.common.core.base.R;
import com.qf.data.entity.coupon.Coupon;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "ken-business-coupon", contextId = "coupon")
public interface CouponFeign {

    @RequestMapping("/coupon/querycoupon")
    R<Coupon> queryCouponById(@RequestParam("cpid") Long cpid);
}

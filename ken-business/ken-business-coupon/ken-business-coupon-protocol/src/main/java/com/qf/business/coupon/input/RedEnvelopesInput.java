package com.qf.business.coupon.input;

import com.qf.business.coupon.vaild.MyRedVaild;
import com.qf.common.web.vaild.CustemVaild;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Accessors(chain = true)
@CustemVaild(vaild = MyRedVaild.class, message = "红包份额与优惠券数量不匹配")
public class RedEnvelopesInput implements Serializable {

    @Max(value = 1, message = "红包类型不正确")
    @Min(value = 0, message = "红包类型不正确")
    @NotNull(message = "红包类型不能为空")
    private Integer redType;

    @Min(value = 1, message = "红包份额至少为1")
    @NotNull(message = "红包份额不能为空")
    private Integer redCount;

    @Min(value = 1, message = "优惠券的数量至少为1")
    @NotNull(message = "红包中优惠券数量不能为空")
    private Integer couponNumber;
}

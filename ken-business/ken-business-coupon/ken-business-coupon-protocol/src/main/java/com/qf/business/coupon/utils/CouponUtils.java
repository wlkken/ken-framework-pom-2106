package com.qf.business.coupon.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qf.business.coupon.rule.ILimit;
import com.qf.business.coupon.rule.IRule;
import com.qf.data.entity.coupon.Coupon;

import java.lang.reflect.Field;

public class CouponUtils {


    /**
     * 通过优惠券对象，解析获取 当前优惠券的限制规则
     * @param coupon
     * @return
     */
    public static ILimit parseCouponGetLimit(Coupon coupon){

        //{"ruleClass":"com.qf.business.coupon.rule.limit.MoneyLimit",
        //   "dyncTemps":[{"name":"mustMoney","title":"满","type":"0","value":"100"}]}
        //限制规则的字符串
        String limitInfo = coupon.getLimitInfo();
        //转换成JSON对象
        JSONObject limitJson = JSON.parseObject(limitInfo);

        //获取限制规则的实现类
        String ruleClass = limitJson.getString("ruleClass");

        ILimit limit = null;
        try {
            //反射获取限制规则的实现类
            Class<?> cls = Class.forName(ruleClass);
            //初始化 限制规则 对象
            limit = (ILimit) cls.newInstance();

            //解析动态参数，反射放入当前的限制规则的对象中
            JSONArray dyncTemps = limitJson.getJSONArray("dyncTemps");
            //循环
            for (int i = 0; i < dyncTemps.size(); i++) {
                //获取每一项的动态属性
                JSONObject temp = dyncTemps.getJSONObject(i);
                //获取动态属性名称
                String fieldName = temp.getString("name");
                //获取动态属性值
                String fieldValue = temp.getString("value");

                //反射放入 限制规则的对象中
                Field declaredField = cls.getDeclaredField(fieldName);
                //设置权限，操作私有 final
                declaredField.setAccessible(true);
                //给该属性设置值
                declaredField.set(limit, fieldValue);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return limit;
    }

    /**
     * 通过优惠券对象，解析获取 当前优惠券的优惠规则
     * @param coupon
     * @return
     */
    public static IRule parseCouponGetRule(Coupon coupon){
        //优惠规则的字符串
        String ruleInfo = coupon.getRuleInfo();
        //转换成JSON对象
        JSONObject ruleJson = JSON.parseObject(ruleInfo);

        //获取优惠规则的实现类
        String ruleClass = ruleJson.getString("ruleClass");

        IRule rule = null;
        try {
            //反射获取限制规则的实现类
            Class<?> cls = Class.forName(ruleClass);
            //初始化 限制规则 对象
            rule = (IRule) cls.newInstance();

            //解析动态参数，反射放入当前的限制规则的对象中
            JSONArray dyncTemps = ruleJson.getJSONArray("dyncTemps");
            //循环
            for (int i = 0; i < dyncTemps.size(); i++) {
                //获取每一项的动态属性
                JSONObject temp = dyncTemps.getJSONObject(i);
                //获取动态属性名称
                String fieldName = temp.getString("name");
                //获取动态属性值
                String fieldValue = temp.getString("value");

                //反射放入 限制规则的对象中
                Field declaredField = cls.getDeclaredField(fieldName);
                //设置权限，操作私有 final
                declaredField.setAccessible(true);
                //给该属性设置值
                declaredField.set(rule, fieldValue);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rule;
    }
}

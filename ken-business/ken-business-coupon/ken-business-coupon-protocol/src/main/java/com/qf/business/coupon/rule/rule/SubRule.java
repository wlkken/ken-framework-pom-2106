package com.qf.business.coupon.rule.rule;

import com.qf.business.coupon.rule.IRule;

import java.math.BigDecimal;

/**
 * 直接扣减的 优惠规则实现类
 */
public class SubRule implements IRule {

    //需要扣减的金额
    private String sub;

    @Override
    public BigDecimal youhui(BigDecimal price) {
        return price.subtract(new BigDecimal(sub)).setScale(2);
    }
}

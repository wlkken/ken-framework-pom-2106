package com.qf.business.coupon.rule;

import java.math.BigDecimal;

/**
 * 限制规则
 */
public interface ILimit {

    /**
     * 参数应该设计成一个可以动态变化（可拓展）的上下文对象
     * @param price
     * @return
     */
    boolean limit(BigDecimal price);
}

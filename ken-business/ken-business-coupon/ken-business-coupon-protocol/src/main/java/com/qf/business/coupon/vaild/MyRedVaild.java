package com.qf.business.coupon.vaild;

import com.qf.business.coupon.input.RedEnvelopesInput;
import com.qf.common.web.vaild.extend.KenVaild;

/**
 * 自定义红包的参数校验器
 */
public class MyRedVaild implements KenVaild<RedEnvelopesInput> {

    @Override
    public boolean vaild(RedEnvelopesInput value) {

        //红包类型
        Integer redType = value.getRedType();
        //红包份额
        Integer redCount = value.getRedCount();
        //优惠券数量
        Integer couponNumber = value.getCouponNumber();

        if (redType == null || redCount == null || couponNumber == null) {
            //直接放行通过
            return true;
        }

        if (redType == 0) {
            //固定红包
            return couponNumber % redCount == 0;
        }

        return couponNumber >= redCount;
    }
}

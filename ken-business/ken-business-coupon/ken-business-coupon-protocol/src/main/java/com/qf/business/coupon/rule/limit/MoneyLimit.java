package com.qf.business.coupon.rule.limit;

import com.qf.business.coupon.rule.ILimit;

import java.math.BigDecimal;

/**
 * 满额限制
 */
public class MoneyLimit implements ILimit {

    private String mustMoney;

    @Override
    public boolean limit(BigDecimal price) {
        return price.doubleValue() >= Double.parseDouble(mustMoney);
    }
}

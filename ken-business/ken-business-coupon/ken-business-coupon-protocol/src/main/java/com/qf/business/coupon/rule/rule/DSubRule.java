package com.qf.business.coupon.rule.rule;

import com.qf.business.coupon.rule.IRule;

import java.math.BigDecimal;

/**
 * 递减优惠的规则
 */
public class DSubRule implements IRule{

    //每满x 减y 最大只能减z
    private String has;
    private String sub;
    private String max;

    @Override
    public BigDecimal youhui(BigDecimal price) {
        //计算扣减的次数
        int s = (int) (price.doubleValue() / Double.parseDouble(has));
        //计算出可以扣减的值
        BigDecimal subDec = BigDecimal.valueOf(s).multiply(new BigDecimal(sub));

        //判断是否超过最大扣减值
        if (subDec.compareTo(new BigDecimal(max)) > 0){
            //超过了最大扣减值
            subDec = new BigDecimal(max);
        }
        return price.subtract(subDec).setScale(2);
    }
}

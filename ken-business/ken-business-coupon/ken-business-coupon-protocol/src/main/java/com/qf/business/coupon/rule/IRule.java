package com.qf.business.coupon.rule;

import java.math.BigDecimal;

/**
 * 优惠规则
 */
public interface IRule {

    /**
     * 参数应该设计成一个可以动态变化（可拓展）的上下文对象
     * @param price
     * @return
     */
    BigDecimal youhui(BigDecimal price);
}

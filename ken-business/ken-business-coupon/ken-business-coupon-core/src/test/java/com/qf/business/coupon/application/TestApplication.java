package com.qf.business.coupon.application;

import com.qf.business.coupon.input.RedEnvelopesInput;
import com.qf.business.coupon.service.RedEnvelopesService;
import com.qf.common.core.utils.UserInfo;
import com.qf.data.entity.auth.vo.WxUserVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestApplication {

    @Autowired
    private RedEnvelopesService redEnvelopesService;

    /**
     * 发红包的性能
     */
    @Test
    public void test(){

        RedEnvelopesInput redEnvelopesInput = new RedEnvelopesInput()
                .setRedType(1)
                .setCouponNumber(50000)
                .setRedCount(200);

        WxUserVo wxUserVo = new WxUserVo();
        wxUserVo.setWuId(3L);
        UserInfo.setUser(wxUserVo);

        long begin = System.currentTimeMillis();
        redEnvelopesService.sendRedEnve(redEnvelopesInput);
        long end = System.currentTimeMillis();
        System.out.println("发红包耗时：" + (end - begin));
    }

    /**
     * 模拟抢红包的业务
     * id为4红包 - 10000张券 - 20份 - 30个人抢 - 10S耗时 - 线程不安全的问题
     * id为5红包 - 10000张券 - 20份 - 30个人抢 - 12S耗时 - 分布式锁 - 线程安全
     * id为14红包 - 50000张券 - 200份 - 250人抢 - 46s耗时
     */
    @Test
    public void test2(){

        long redId = 21L;

        CountDownLatch countDownLatch = new CountDownLatch(250);

        long begin = System.currentTimeMillis();

        for (int i = 0; i < 250; i++) {
            final int index = i;
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "开始前红包!" );

                //模拟用户信息
                WxUserVo wxUserVo = new WxUserVo();
                wxUserVo.setWuId(index + 7L);
                UserInfo.setUser(wxUserVo);

                //抢红包的业务
                redEnvelopesService.robRedEnve(redId);
                System.out.println(Thread.currentThread().getName() + "抢红包结束！");

                countDownLatch.countDown();
            }).start();
        }

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("250个人抢红包的耗时：" + (end - begin));
    }
}

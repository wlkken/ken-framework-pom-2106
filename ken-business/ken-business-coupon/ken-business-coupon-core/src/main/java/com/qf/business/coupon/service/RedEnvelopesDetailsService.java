package com.qf.business.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.coupon.RedEnvelopesDetails;

import java.util.List;

/**
 * 红包明细表(RedEnvelopesDetails)表服务接口
 *
 * @author makejava
 * @since 2021-12-06 14:50:19
 */
public interface RedEnvelopesDetailsService extends IService<RedEnvelopesDetails> {

    List<RedEnvelopesDetails> queryRedEnvelopesDetails(Long redId, Integer limit);

    int updateRedDetailShareId(List<Long> detilsId, Long shareId);

    int insertBatch(List<RedEnvelopesDetails> redEnvelopesDetails);
}

package com.qf.business.coupon.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.coupon.service.RedEnvelopesShareService;
import com.qf.data.entity.coupon.RedEnvelopesShare;
import com.qf.data.mapper.coupon.RedEnvelopesShareDao;
import org.springframework.stereotype.Service;

/**
 * 红包份额表(RedEnvelopesShare)表服务实现类
 *
 * @author makejava
 * @since 2021-12-06 14:50:46
 */
@Service
public class RedEnvelopesShareServiceImpl extends ServiceImpl<RedEnvelopesShareDao, RedEnvelopesShare> implements RedEnvelopesShareService {

}

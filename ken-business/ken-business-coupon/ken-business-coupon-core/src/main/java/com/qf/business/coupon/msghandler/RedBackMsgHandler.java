package com.qf.business.coupon.msghandler;

import com.qf.business.coupon.service.RedEnvelopesService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class RedBackMsgHandler {

    @Autowired
    private RedEnvelopesService redEnvelopesService;

    @RabbitListener(queues = "coupon-dead-queue")
    public void handler(Long redId){
        System.out.println("接收到红包过期的消息：" + redId);
        redEnvelopesService.redBack(redId);
    }

}

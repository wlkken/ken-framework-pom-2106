package com.qf.business.coupon.application;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitMQConfig {

    @Bean
    public FanoutExchange getExchange(){
        return new FanoutExchange("coupon-exchange");
    }

    @Bean
    public Queue getQueue(){
        Map<String, Object> map = new HashMap<>();
        map.put("x-message-ttl", 1000 * 15);//设置队列消息的过期时间
        map.put("x-dead-letter-exchange", "coupon-dead-exchange");//设置队列的死信交换机
        return new Queue("coupon-queue", true, false, false, map);
    }

    /**
     * 死信交换机
     * @return
     */
    @Bean
    public FanoutExchange getFanoutExchangeDead(){
        return new FanoutExchange("coupon-dead-exchange");
    }

    /**
     * 死信队列
     * @return
     */
    @Bean
    public Queue getQueueDead(){
        return new Queue("coupon-dead-queue");
    }

    @Bean
    public Binding getBindingBuilder1(FanoutExchange getExchange, Queue getQueue){
        return BindingBuilder.bind(getQueue).to(getExchange);
    }

    @Bean
    public Binding getBindingBuilder2(FanoutExchange getFanoutExchangeDead, Queue getQueueDead){
        return BindingBuilder.bind(getQueueDead).to(getFanoutExchangeDead);
    }
}

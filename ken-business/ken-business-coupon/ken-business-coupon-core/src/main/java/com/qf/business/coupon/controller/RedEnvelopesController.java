package com.qf.business.coupon.controller;

import com.qf.business.coupon.input.RedEnvelopesInput;
import com.qf.business.coupon.service.RedEnvelopesService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/open/api/red")
public class RedEnvelopesController {

    @Autowired
    private RedEnvelopesService redEnvelopesService;


    /**
     * 发送红包
     * @return
     */
    @RequestMapping("/send")
    public R sendRedEnv(@Valid RedEnvelopesInput input){
        long begin = System.currentTimeMillis();
        Long redId = redEnvelopesService.sendRedEnve(input);
        long end = System.currentTimeMillis();
        System.out.println("发送红包耗时：" + (end - begin) + "ms");
        return RetUtils.createSucc(redId);
    }

    /**
     * 抢红包的逻辑
     * @return
     */
    @RequestMapping("/rob")
    public R robRedEnv(Long redId){

        //调用抢红包的业务
        int number = redEnvelopesService.robRedEnve(redId);

        return RetUtils.createSucc(number);
    }
}

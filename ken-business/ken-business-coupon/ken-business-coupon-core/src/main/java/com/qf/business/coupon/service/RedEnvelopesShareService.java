package com.qf.business.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.coupon.RedEnvelopesShare;

/**
 * 红包份额表(RedEnvelopesShare)表服务接口
 *
 * @author makejava
 * @since 2021-12-06 14:50:46
 */
public interface RedEnvelopesShareService extends IService<RedEnvelopesShare> {

}

package com.qf.business.coupon.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.coupon.service.CouponTemplateService;
import com.qf.data.entity.coupon.CouponTemplate;
import com.qf.data.mapper.coupon.CouponTemplateDao;
import org.springframework.stereotype.Service;

/**
 * (CouponTemplate)表服务实现类
 *
 * @author makejava
 * @since 2021-12-01 11:15:08
 */
@Service("couponTemplateService")
public class CouponTemplateServiceImpl extends ServiceImpl<CouponTemplateDao, CouponTemplate> implements CouponTemplateService {

}

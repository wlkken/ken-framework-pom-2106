package com.qf.business.coupon.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.coupon.service.RedEnvelopesDetailsService;
import com.qf.data.entity.coupon.RedEnvelopesDetails;
import com.qf.data.mapper.coupon.RedEnvelopesDetailsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 红包明细表(RedEnvelopesDetails)表服务实现类
 *
 * @author makejava
 * @since 2021-12-06 14:50:19
 */
@Service
public class RedEnvelopesDetailsServiceImpl extends ServiceImpl<RedEnvelopesDetailsDao, RedEnvelopesDetails> implements RedEnvelopesDetailsService {

    @Autowired
    private RedEnvelopesDetailsDao redEnvelopesDetailsDao;

    @Override
    public List<RedEnvelopesDetails> queryRedEnvelopesDetails(Long redId, Integer limit) {
        return redEnvelopesDetailsDao.queryRedEnvelopesDetails(redId, limit);
    }

    @Override
    public int updateRedDetailShareId(List<Long> detilsId, Long shareId) {
        return redEnvelopesDetailsDao.updateRedDetailShareId(detilsId, shareId);
    }

    /**
     * 批量插入
     * @param redEnvelopesDetails
     * @return
     */
    @Override
    @Transactional
    public int insertBatch(List<RedEnvelopesDetails> redEnvelopesDetails) {
        return redEnvelopesDetailsDao.insertBatch(redEnvelopesDetails);
    }
}

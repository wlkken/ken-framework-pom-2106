package com.qf.business.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.business.coupon.input.RedEnvelopesInput;
import com.qf.data.entity.coupon.RedEnvelopes;

/**
 * 红包表(RedEnvelopes)表服务接口
 *
 * @author makejava
 * @since 2021-12-06 14:49:56
 */
public interface RedEnvelopesService extends IService<RedEnvelopes> {

    /**
     * 发送红包
     * @param params
     * @return
     */
    Long sendRedEnve(RedEnvelopesInput params);

    /**
     * 抢红包
     * @return
     */
    int robRedEnve(Long redId);

    /**
     * 处理红包回退
     * @param redId
     * @return
     */
    int redBack(Long redId);
}

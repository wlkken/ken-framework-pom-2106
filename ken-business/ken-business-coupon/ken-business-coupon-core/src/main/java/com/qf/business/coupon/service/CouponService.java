package com.qf.business.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.business.hotel.protocol.input.OrderPriceInput;
import com.qf.data.entity.coupon.Coupon;

import java.util.List;

/**
 * (Coupon)表服务接口
 *
 * @author makejava
 * @since 2021-12-01 11:15:37
 */
public interface CouponService extends IService<Coupon> {

    /**
     * 用户领取优惠券
     * @return
     */
    int userGetCoupon(Long cpid);

    /**
     * 用户拥有优惠券的列表
     * @return
     */
    List<Coupon> userCouponList(Integer status);

    /**
     * 根据用户以及订单明细查询优惠券列表
     * @param orderPriceInput
     * @return
     */
    List<List<Coupon>> userOrdersList(OrderPriceInput orderPriceInput);
}

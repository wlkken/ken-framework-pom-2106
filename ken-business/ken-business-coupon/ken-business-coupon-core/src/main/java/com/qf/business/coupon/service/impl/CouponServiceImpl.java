package com.qf.business.coupon.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.coupon.rule.ILimit;
import com.qf.business.coupon.service.CouponService;
import com.qf.business.coupon.service.CouponUserService;
import com.qf.business.coupon.utils.CouponUtils;
import com.qf.business.hotel.protocol.input.OrderPriceInput;
import com.qf.business.hotel.protocol.output.OrderPriceOutput;
import com.qf.common.core.base.R;
import com.qf.common.core.utils.UserInfo;
import com.qf.data.entity.auth.vo.WxUserVo;
import com.qf.data.entity.coupon.Coupon;
import com.qf.data.entity.coupon.CouponUser;
import com.qf.data.mapper.coupon.CouponDao;
import com.qf.feign.business.hotel.HotelFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * (Coupon)表服务实现类
 *
 * @author makejava
 * @since 2021-12-01 11:15:37
 */
@Service("couponService")
public class CouponServiceImpl extends ServiceImpl<CouponDao, Coupon> implements CouponService {

    @Autowired
    private CouponDao couponDao;

    @Autowired
    private HotelFeign hotelFeign;

    @Autowired
    private CouponUserService couponUserService;

    /**
     * 用户领取优惠券
     * @param cpid
     * @return
     */
    @Override
    public int userGetCoupon(Long cpid) {

        //判断券ID是否合法
        Coupon coupon = couponDao.selectById(cpid);
        Assert.notNull(coupon, "优惠券不存在！");

        //获取当前用户信息
        WxUserVo user = UserInfo.getUser();
        Assert.notNull(user, "用户信息不存在！");

        //
        CouponUser couponUser = new CouponUser()
                .setUId(user.getWuId())
                .setCpId(cpid)
                .setNumber(1)
                .setEndTime(null);

        couponUserService.save(couponUser);
        return 1;
    }

    /**
     * 用户拥有优惠券的列表
     * @return
     */
    @Override
    public List<Coupon> userCouponList(Integer status) {
        //获取当前用户信息
        WxUserVo user = UserInfo.getUser();
        Assert.notNull(user, "用户信息不存在！");

        List<Coupon> coupons = couponDao.queryByUser(user.getWuId(), status);
        return coupons;
    }

    /**
     * 根据用户以及订单明细 查询优惠券列表
     * @param orderPriceInput
     * @return
     */
    @Override
    public List<List<Coupon>> userOrdersList(OrderPriceInput orderPriceInput) {

        //查询当前用户所有的优惠券
        List<Coupon> coupons = this.userCouponList(null);

        //能够使用的优惠券列表
        List<Coupon> canUser = new ArrayList<>();
        //不可用的优惠券列表
        List<Coupon> unCanUser = new ArrayList<>();

        List<List<Coupon>> resultList = new ArrayList<>();
        resultList.add(canUser);
        resultList.add(unCanUser);

        //获取订单的总金额
        OrderPriceOutput data = null;
        R<OrderPriceOutput> orderPrices = hotelFeign.getOrderPricesFeign(orderPriceInput);
        if(orderPrices != null && orderPrices.getCode() == 200) {
            data = orderPrices.getData();
        }
        Assert.notNull(data, "获取订单价格信息有误！");

        //循环优惠券列表，判断是否可用
        for (Coupon coupon : coupons) {
            //获取优惠券的 使用限制规则
            ILimit limit = CouponUtils.parseCouponGetLimit(coupon);

            //判断当前优惠券是否可用
            if(limit.limit(data.getAllprice())){
                //可用
                canUser.add(coupon);
            } else {
                //不可用
                unCanUser.add(coupon);
            }
        }

        return resultList;
    }
}

package com.qf.business.coupon.controller;

import com.qf.business.coupon.service.CouponService;
import com.qf.business.hotel.protocol.input.OrderPriceInput;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.data.entity.coupon.Coupon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("/coupon")
public class ConponController {

    @Autowired
    private CouponService couponService;

    @PostConstruct
    public void init(){
        System.out.println(couponService.getClass().getName());
    }

    /**
     * 查询优惠券列表
     * @return
     */
    @RequestMapping("/list")
    public R list(){
        System.out.println("查询优惠券列表！");
        List<Coupon> coupons = couponService.list();
        return RetUtils.createSucc(coupons);
    }

    /**
     * 查询指定用户拥有的券列表
     * @return
     */
    @RequestMapping("/listByUser")
    public R userList(){
        List<Coupon> coupons = couponService.userCouponList(0);
        return RetUtils.createSucc(coupons);
    }

    /**
     * 根据用户id 以及 订单相关参数，查询优惠券列表
     * @return
     */
    @RequestMapping("/listByUserOrders")
    public R<List<List<Coupon>>> userOrdersList(OrderPriceInput orderPriceInput){
        List<List<Coupon>> coupons = couponService.userOrdersList(orderPriceInput);
        return RetUtils.createSucc(coupons);
    }

    /**
     * 新增优惠券
     * @return
     */
    @RequestMapping("/insert")
    public R insert(Coupon coupon){
        couponService.save(coupon);
        return RetUtils.createSucc(true);
    }

    /**
     * 领取优惠券 - 前端用户
     * @return
     */
    @RequestMapping("/getcoupon")
    public R getCoupon(Long cpid){
        couponService.userGetCoupon(cpid);
        return RetUtils.createSucc(true);
    }

    /**
     * 根据优惠券id查询优惠券信息
     * @param cpid
     * @return
     */
    @RequestMapping("/querycoupon")
    public R<Coupon> queryCouponById(Long cpid){
        Coupon coupon = couponService.getById(cpid);
        return RetUtils.createSucc(coupon);
    }
}

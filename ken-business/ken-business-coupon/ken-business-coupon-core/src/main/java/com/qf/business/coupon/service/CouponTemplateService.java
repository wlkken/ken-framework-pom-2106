package com.qf.business.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.coupon.CouponTemplate;

/**
 * (CouponTemplate)表服务接口
 *
 * @author makejava
 * @since 2021-12-01 11:15:08
 */
public interface CouponTemplateService extends IService<CouponTemplate> {

}

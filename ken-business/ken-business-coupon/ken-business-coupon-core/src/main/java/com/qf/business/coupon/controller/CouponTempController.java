package com.qf.business.coupon.controller;

import com.alibaba.fastjson.JSON;
import com.qf.business.coupon.service.CouponTemplateService;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.data.entity.coupon.CouponTemplate;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 券模板管理
 */
@RestController
@RequestMapping("/coupon/temp")
public class CouponTempController {

    @Autowired
    private CouponTemplateService couponTemplateService;

    //查询所有的模板列表
    @RequestMapping("/list")
    public R<List<CouponTemplate>> queryAll(){
        List<CouponTemplate> list = couponTemplateService.list();
        return RetUtils.createSucc(list);
    }

    /**
     * 模板的上传
     *
     * xml -> Document -> 解析
     * 生成xml
     *
     * @return
     */
    @RequestMapping("/upload")
    public R tempUpdate(MultipartFile file) throws Exception {
        //解析规则模板 - xml解析 json解析 excel解析 dom4j
        SAXReader reader = new SAXReader();
        //将xml的来源转换成document对象
        Document document = reader.read(file.getInputStream());
        //获取xml的根节点
        Element root = document.getRootElement();
//        String name = root.getName();
//        System.out.println("根节点的名称：" + name);

        //通过根节点获取子节点
        //通过某个节点获取固定名称的子节点 - root.element("hello");
        //通过某个节点获取固定名称的子节点集合 - root.elements("hello");
        //通过某个节点获取所有子节点的集合 - root.elements();

        //获取模板名称
        Element templateName = root.element("templateName");
        String templateNameText = templateName.getText();
        System.out.println("获取模板名称：" + templateNameText);

        //获取模板类型
        Element type = root.element("type");
        String typeText = type.getText();
        System.out.println("获取模板类型：" + typeText);

        //获取对应的实现类
        Element classPath = root.element("classPath");
        String classPathText = classPath.getText();
        System.out.println("获取规则对应的实现类全路径：" + classPathText);

        //获取对应的动态字段集合
        List<Map<String, Object>> mapList = new ArrayList<>();

        Element dyncTemplate = root.element("dyncTemplate");
        List<Element> inputList = dyncTemplate.elements("input");
        for (Element inputElement : inputList) {
            String inputTitle = inputElement.attribute("title").getValue();
            String inputName = inputElement.attribute("name").getValue();
            String inputType = inputElement.attribute("type").getValue();

            Map<String, Object> map = new HashMap<>();
            map.put("title", inputTitle);
            map.put("name", inputName);
            map.put("type", inputType);
            mapList.add(map);
//            System.out.println("动态属性：" + inputTitle + " -- " + inputName + " -- " + inputType);
        }

        //将Xml解析出来的数据转换成模板对象
        CouponTemplate couponTemplate = new CouponTemplate()
                .setTemplateName(templateNameText)
                .setType(Integer.parseInt(typeText))
                .setClassPath(classPathText)
                .setDyncTemplate(JSON.toJSONString(mapList));


        //保存到数据库中
        couponTemplateService.save(couponTemplate);

        return RetUtils.createSucc(true);
    }
}

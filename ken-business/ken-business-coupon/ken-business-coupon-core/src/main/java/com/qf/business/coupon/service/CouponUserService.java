package com.qf.business.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.data.entity.coupon.CouponUser;

import java.util.List;

/**
 * (CouponUser)表服务接口
 *
 * @author makejava
 * @since 2021-12-01 11:15:59
 */
public interface CouponUserService extends IService<CouponUser> {

    int updateCouponUserStatus(List<Long> cuIds, Integer status);

    List<CouponUser> queryCouponUserByUidAndLimit(Long uid, Integer limit);

    int insertBatch(List<CouponUser> couponUsers);
}

package com.qf.business.coupon.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.business.coupon.service.CouponUserService;
import com.qf.data.entity.coupon.CouponUser;
import com.qf.data.mapper.coupon.CouponUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (CouponUser)表服务实现类
 *
 * @author makejava
 * @since 2021-12-01 11:15:59
 */
@Service
public class CouponUserServiceImpl extends ServiceImpl<CouponUserDao, CouponUser> implements CouponUserService {

    @Autowired
    private CouponUserDao couponUserDao;

    /**
     * 修改对应的 用户领取记录 的状态
     * @param cuIds
     * @param status
     * @return
     */
    @Override
    public int updateCouponUserStatus(List<Long> cuIds, Integer status) {
        return couponUserDao.updateStatus(cuIds, status);
    }

    /**
     * 查询用户的优惠券领取记录，并且只取前limit条
     * @param uid
     * @param limit
     * @return
     */
    @Override
    public List<CouponUser> queryCouponUserByUidAndLimit(Long uid, Integer limit) {
        return couponUserDao.queryCouponUserByUidAndLimit(uid, limit);
    }

    /**
     * 批量插入红包领取记录
     * @param couponUsers
     * @return
     */
    @Override
    public int insertBatch(List<CouponUser> couponUsers) {
        return couponUserDao.insertBatch(couponUsers);
    }
}

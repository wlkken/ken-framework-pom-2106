package com.qf.business.search.input;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 搜索条件
 */
@Data
public class SearchParamsInput implements Serializable {

    private String cityName;
    private String keyword;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;
    private Double lat;
    private Double lon;
}

package com.qf.business.searc.handler;

import com.qf.business.searc.service.ISearchService;
import com.qf.common.event.consumer.EventType;
import com.qf.common.event.consumer.IEventHandler;
import com.qf.data.entity.hotel.CRoom;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 客房消息的处理器
 */
@EventType(type = "room-insert")
public class RoomMsgHandler implements IEventHandler<CRoom> {

    @Autowired
    private ISearchService searchService;

    @Override
    public void eventHandler(CRoom event) {
        searchService.insertRoom(event);
    }
}

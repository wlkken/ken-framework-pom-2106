package com.qf.business.searc.controller;

import com.qf.business.searc.service.ISearchService;
import com.qf.business.search.input.SearchParamsInput;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import com.qf.data.entity.hotel.dto.CHotelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/open/api/hotel")
public class HotelSearchController {

    @Autowired
    private ISearchService searchService;

    /**
     * 搜索酒店信息
     * @return
     */
    @RequestMapping("/search")
    public R hotelSearch(SearchParamsInput paramsInput){
        List<CHotelDto> cHotelDtos = searchService.searchHotel(paramsInput);
        return RetUtils.createSucc(cHotelDtos);
    }
}

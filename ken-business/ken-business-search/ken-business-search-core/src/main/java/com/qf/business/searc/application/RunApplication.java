package com.qf.business.searc.application;

import com.qf.business.searc.service.ISearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 启动时触发
 */
@Component
@Slf4j
public class RunApplication implements CommandLineRunner {

    @Autowired
    private ISearchService searchService;

    @Override
    public void run(String... args) throws Exception {
        if (!searchService.existsIndex()) {
            //如果索引库不存在则创建
            boolean flag = searchService.createIndex();
            log.debug("[Index-Create] - 创建索引库！{}", flag);
        } else {
            log.debug("[Index-Create] - 索引库已经存在！");
        }
    }
}

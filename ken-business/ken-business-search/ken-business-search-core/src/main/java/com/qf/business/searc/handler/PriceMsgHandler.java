package com.qf.business.searc.handler;

import com.qf.business.searc.service.ISearchService;
import com.qf.common.event.consumer.EventType;
import com.qf.common.event.consumer.IEventHandler;
import com.qf.data.entity.hotel.CPrice;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@EventType(type = "price-insert")
public class PriceMsgHandler implements IEventHandler<List<CPrice>> {

    @Autowired
    private ISearchService searchService;

    @Override
    public void eventHandler(List<CPrice> event) {
        System.out.println("接收到价格列表：" + event);
        for (CPrice cPrice : event) {
            searchService.insertRoomPrice(cPrice.getHid().intValue(), cPrice);
        }
    }
}

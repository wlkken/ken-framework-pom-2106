package com.qf.business.searc.job;

import com.qf.business.searc.service.ISearchService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Map;

@Component
@Slf4j
public class MyJob {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ISearchService searchService;

    /**
     * 设置执行器的任务处理方法
     *
     * 10W
     */
    @XxlJob("djlJob")
    public void djlJob(){
        log.debug("[sync-djl-es] - 同步点击量数据到es中....");
        //获取点击量
        Map<Object, Object> entries = redisTemplate.opsForHash().entries("hotel-djl");
        if (!CollectionUtils.isEmpty(entries)) {
            for (Map.Entry<Object, Object> entry : entries.entrySet()) {
                //获得酒店id号
                Long hid = Long.parseLong(entry.getKey().toString());
                //获得点击量
                Integer djl = Integer.parseInt(entry.getValue().toString());
                //调用业务层完成点击量的新增
                searchService.incrHotelDjl(hid, djl);
            }
            //清空redis
            redisTemplate.delete("hotel-djl");
        }
    }
}

package com.qf.business.searc.application;

import com.qf.common.event.annotation.EnableEventConsumer;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEventConsumer
//@EnableScheduling
@EnableConfigurationProperties(XxlJobProperties.class)
public class SearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearchApplication.class, args);
    }

    @Autowired
    private XxlJobProperties xxlJobProperties;

    /**
     * xxljob和spring整合的装配类
     * @return
     */
    @Bean
    public XxlJobSpringExecutor getXxlJobExecutor(){
        XxlJobSpringExecutor executor = new XxlJobSpringExecutor();
        executor.setAdminAddresses(xxlJobProperties.getAdminAddresses());
        executor.setAppname(xxlJobProperties.getAppname());
        executor.setIp(xxlJobProperties.getIp());
        executor.setPort(xxlJobProperties.getPort());
        executor.setAccessToken(xxlJobProperties.getAccessToken());
        executor.setLogPath(xxlJobProperties.getLogPath());
        executor.setLogRetentionDays(xxlJobProperties.getLogRetentionDays());
        return  executor;
    }
}

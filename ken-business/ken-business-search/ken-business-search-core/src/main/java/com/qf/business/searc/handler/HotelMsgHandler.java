package com.qf.business.searc.handler;

import com.qf.business.searc.service.ISearchService;
import com.qf.common.event.consumer.EventType;
import com.qf.common.event.consumer.IEventHandler;
import com.qf.data.entity.hotel.CHotel;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理酒店新增的事件
 */
@EventType(type = "hotel-insert")
public class HotelMsgHandler implements IEventHandler<CHotel> {

    @Autowired
    private ISearchService searchService;

    @Override
    public void eventHandler(CHotel event) {
        //交给业务层处理
        searchService.insertHotel(event);
    }
}

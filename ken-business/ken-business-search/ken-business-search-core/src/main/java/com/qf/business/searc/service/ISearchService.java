package com.qf.business.searc.service;

import com.qf.business.search.input.SearchParamsInput;
import com.qf.data.entity.hotel.CHotel;
import com.qf.data.entity.hotel.CPrice;
import com.qf.data.entity.hotel.CRoom;
import com.qf.data.entity.hotel.dto.CHotelDto;

import java.util.List;

public interface ISearchService {

    boolean createIndex();

    boolean existsIndex();

    int insertHotel(CHotel cHotel);

    int insertRoom(CRoom cRoom);

    int insertRoomPrice(Integer hid, CPrice cPrice);

    int updateRoomPrice(Integer hid, CPrice cPrice);

    List<CHotelDto> searchHotel(SearchParamsInput paramsInput);

    int incrHotelDjl(Long hid, Integer djl);
}

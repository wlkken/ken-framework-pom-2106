package com.qf.business.searc.handler;

import com.qf.business.searc.service.ISearchService;
import com.qf.common.event.consumer.EventType;
import com.qf.common.event.consumer.IEventHandler;
import com.qf.data.entity.hotel.CPrice;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 修改酒店客房的某一天的价格
 */
@EventType(type = "update-price")
public class PriceUpdateMsgHandler implements IEventHandler<CPrice> {

    @Autowired
    private ISearchService searchService;

    @Override
    public void eventHandler(CPrice event) {
        searchService.updateRoomPrice(event.getHid().intValue(), event);
    }
}

package com.qf.common.web.vaild;

import com.qf.common.web.vaild.constraint.CustemConstraint;
import com.qf.common.web.vaild.extend.KenVaild;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 自定义参数校验注解
 */
@Documented
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CustemConstraint.class)
public @interface CustemVaild {

    /**
     * 校验失败后返回的字符串
     * @return
     */
    String message() default "";

    /**
     * 校验分组
     * @return
     */
    Class<?>[] groups() default { };

    /**
     * 校验负载
     * @return
     */
    Class<? extends Payload>[] payload() default { };

    /**
     * 自定义的校验实现类类型
     * @return
     */
    Class<? extends KenVaild> vaild();
}

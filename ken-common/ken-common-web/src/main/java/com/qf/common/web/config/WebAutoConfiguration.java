package com.qf.common.web.config;

import com.qf.common.web.exception.GlobalException;
import com.qf.common.web.interceptor.register.InterceptroRegister;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = {"com.qf.business", "com.qf.ability", "com.qf.common.web.interceptor"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.qf.feign")
public class WebAutoConfiguration {

    /**
     * 配置全局异常处理工具类
     * @return
     */
    @Bean
    public GlobalException getGlobalException(){
        return new GlobalException();
    }

    /**
     * 装配 自定义的拦截器注册对象
     * @return
     */
    @Bean
    public InterceptroRegister getInterceptroRegister(){
        return new InterceptroRegister();
    }
}

package com.qf.common.web.vaild.constraint;

import com.qf.common.web.vaild.CustemVaild;
import com.qf.common.web.vaild.extend.KenVaild;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 自定义的参数校验方法
 */
public class CustemConstraint implements ConstraintValidator<CustemVaild, Object> {

    private CustemVaild custemVaild;

    @Override
    public void initialize(CustemVaild custemVaild) {
        this.custemVaild = custemVaild;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        try {
            //如果校验的值/对象为null，则放行
            if (value == null) return true;
            KenVaild kenVaild = custemVaild.vaild().newInstance();
            return kenVaild.vaild(value);
        } catch (Exception e) {
        }
        return true;
    }
}

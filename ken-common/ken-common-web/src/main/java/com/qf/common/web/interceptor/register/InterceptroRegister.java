package com.qf.common.web.interceptor.register;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import java.util.List;

/**
 * 拦截器的注册
 */
public class InterceptroRegister implements WebMvcConfigurer {

    @Autowired(required = false)
    private List<HandlerInterceptorAdapter> interceptorAdapters;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (interceptorAdapters != null) {
            for (HandlerInterceptorAdapter interceptorAdapter : interceptorAdapters) {
                registry.addInterceptor(interceptorAdapter);
            }
        }
    }
}

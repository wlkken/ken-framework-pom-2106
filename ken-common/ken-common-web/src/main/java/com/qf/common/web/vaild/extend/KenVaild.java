package com.qf.common.web.vaild.extend;

/**
 * 校验拓展接口
 */
public interface KenVaild<T> {

    /**
     * 自定义的校验方法
     * @return
     */
    boolean vaild(T value);
}

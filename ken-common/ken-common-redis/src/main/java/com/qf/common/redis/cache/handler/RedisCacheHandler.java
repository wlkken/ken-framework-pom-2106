package com.qf.common.redis.cache.handler;

import com.qf.common.redis.cache.myinter.ClusterCacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * 分布式缓存处理器
 */
public class RedisCacheHandler extends ClusterCacheHandler {

    @Autowired
    private RedisTemplate redisTemplate;

    @PostConstruct
    public void init(){
        System.out.println("注入的对象：" + redisTemplate.hashCode());
    }

    @Override
    public boolean putCache(String key, Serializable value) {
        return this.putCache(key, value, 5, TimeUnit.MINUTES);
    }

    @Override
    public boolean putCache(String key, Serializable value, long timeout, TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value, timeout, unit);
        return true;
    }

    @Override
    public Serializable getCache(String key) {
        return (Serializable) redisTemplate.opsForValue().get(key);
    }

    @Override
    public Serializable delCache(String key) {
        return redisTemplate.delete(key);
    }
}

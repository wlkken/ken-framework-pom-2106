package com.qf.common.redis.cache.aop;

import com.qf.common.core.utils.SpelUtils;
import com.qf.common.redis.cache.annotation.CacheDel;
import com.qf.common.redis.cache.annotation.CacheGet;
import com.qf.common.redis.cache.myinter.ClusterCacheHandler;
import com.qf.common.redis.cache.myinter.MemoryCacheHandler;
import com.qf.common.redis.utils.LockUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

@Component
@Aspect
@Slf4j
public class CacheAop {

    @Autowired
    private MemoryCacheHandler cacheHandler;

    @Autowired
    private ClusterCacheHandler clusterCacheHandler;

    /**
     * 缓存获取的增强方法
     * @return
     */
    @Around("@annotation(com.qf.common.redis.cache.annotation.CacheGet)")
    public Object cacheGetHandler(ProceedingJoinPoint joinPoint) throws Throwable {

        //获取缓存的名字 - key（通过@CacheGet注解）
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        CacheGet cacheGet = method.getAnnotation(CacheGet.class);

        //超时时间
        long timeout = cacheGet.timeout();
        TimeUnit unit = cacheGet.unit();
        boolean random = cacheGet.isRandom();

        //是否需要随机过期时间
        timeout = unit.toSeconds(timeout);
        if (random) {
            timeout += (long)(Math.random() * 300);
        }

        //获取condition 是否需要进行缓存的操作
        String condition = cacheGet.condition();
        boolean conditionFlag = SpelUtils.parse(condition, method, joinPoint.getArgs(), true);

        String cacheName = cacheGet.cacheName();
        String key = cacheGet.key();//spel表达式

        //解析spel表达式
        key = SpelUtils.parse(key, method, joinPoint.getArgs(), null);

        //获取缓存的名称
        String cacheKey = cacheName + "::" + key;
        log.info("[cache-get] - 获取到缓存的key值 - {}", cacheKey);

        //通过key获取本地缓存的值，如果不为空就直接返回
        Serializable value = conditionFlag ? cacheHandler.getCache(cacheKey) : null;
        log.info("[cache-get] - 获取本地缓存的结果 - {}", value);

        //如果不为null，说明有值，就直接返回
        if (value != null) {
            return value;
        }

        //通过key获取分布式缓存的值，如果不为就直接返回
        value = conditionFlag ? clusterCacheHandler.getCache(cacheKey) : null;
        log.info("[cache-get] - 获取分布式缓存的结果 - {}", value);

        //如果不为null，说明有值，就直接返回
        if (value != null) {
            //本地缓存的缓存重建
            cacheHandler.putCache(cacheKey, value, timeout, TimeUnit.SECONDS);
            return value;
        }

        //放行请求
        try {
            //添加分布式锁，进行缓存重建，保证只有一个线程
            LockUtils.lock("lock-" + cacheKey);

            //判断缓存中是否存在数据
            value = conditionFlag ? cacheHandler.getCache(cacheKey) : null;
            log.info("[cache-get] - 获取本地缓存的结果 - {}", value);

            //如果value还是为null就进行目标方法的执行
            if (value == null) {
                value = (Serializable) joinPoint.proceed();
                log.info("[cache-get] - 调用目标方法获得结果 - {}", value);

                //获取目标方法的返回值
                if (value != null && conditionFlag) {
                    //缓存重建
                    clusterCacheHandler.putCache(cacheKey, value, timeout, TimeUnit.SECONDS);
                    cacheHandler.putCache(cacheKey, value, timeout, TimeUnit.SECONDS);
                }
            }
        } catch (Throwable throwable) {
            throw throwable;
        } finally {
            //解除分布式锁
            LockUtils.unlock();
        }

        //将目标方法的返回值 返回
        return value;
    }

    /**
     * 缓存删除的增强方法
     * @return
     */
    @Around("@annotation(com.qf.common.redis.cache.annotation.CacheDel)")
    public Object cacheDelHandler(ProceedingJoinPoint joinPoint) throws Throwable {

        //获取注解
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        CacheDel cacheDel = method.getAnnotation(CacheDel.class);

        //通过获取缓存的key
        String cacheName = cacheDel.cacheName();
        String key = cacheDel.key();//spel表达式

        //解析spel表达式
        key = SpelUtils.parse(key, method, joinPoint.getArgs(), null);

        //获取缓存的名称
        String cacheKey = cacheName + "::" + key;
        log.info("[cache-del] - 获取到缓存的key值 - {}", cacheKey);

        //获取缓存的相关参数
        boolean isBefore = cacheDel.beforeInvcation();

        //前置删除缓存
        if (isBefore) {
//            cacheHandler.delCache(cacheKey);
            cacheHandler.syncDelMemoryCache(cacheKey);
            clusterCacheHandler.delCache(cacheKey);
            log.info("[cache-del] - 前置删除缓存 - {}", cacheKey);
        }

        //调用目标方法
        Object result = null;
        try {
            result = joinPoint.proceed();
            log.info("[cache-del] - 调用目标方法");
        } catch (Throwable e) {
            throw e;
        }

        //删除缓存
        if (!isBefore) {
//            cacheHandler.delCache(cacheKey);
            cacheHandler.syncDelMemoryCache(cacheKey);
            clusterCacheHandler.delCache(cacheKey);
            log.info("[cache-del] - 后置删除缓存 - {}", cacheKey);
        }
        return result;
    }
}

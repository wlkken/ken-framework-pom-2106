package com.qf.common.redis.cache.myinter.base;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * 基础缓存处理接口
 */
public interface IBaseCacheHandler<T extends Serializable> {

    /**
     * 新增缓存
     * @param key
     * @param value
     * @return
     */
    boolean putCache(String key, T value);

    /**
     * 新增缓存并且设置缓存的过期时间
     * @param key
     * @param value
     * @param timeout
     * @param unit
     * @return
     */
    boolean putCache(String key, T value, long timeout, TimeUnit unit);

    /**
     * 获取缓存
     * @param key
     * @return
     */
    T getCache(String key);

    /**
     * 删除缓存
     */
    T delCache(String key);
}

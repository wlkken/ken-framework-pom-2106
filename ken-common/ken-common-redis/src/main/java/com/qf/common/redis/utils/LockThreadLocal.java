package com.qf.common.redis.utils;

import org.redisson.api.RLock;

public class LockThreadLocal {

    private static ThreadLocal<RLock> rLockThreadLocal = new ThreadLocal<>();

    public static void setLock(RLock rLock){
        LockThreadLocal.rLockThreadLocal.set(rLock);
    }

    public static RLock getLock(){
        return LockThreadLocal.rLockThreadLocal.get();
    }

    public static void clear(){
        LockThreadLocal.rLockThreadLocal.remove();
    }
}

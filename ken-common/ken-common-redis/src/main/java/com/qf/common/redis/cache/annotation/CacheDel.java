package com.qf.common.redis.cache.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CacheDel {

    String cacheName() default "";

    String key() default "";

    boolean beforeInvcation() default false;
}

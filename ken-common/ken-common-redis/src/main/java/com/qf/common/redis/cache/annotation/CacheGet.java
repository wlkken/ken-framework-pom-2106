package com.qf.common.redis.cache.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CacheGet {

    String cacheName() default "";

    String key() default "";

    String condition() default "true";

    long timeout() default 5L;

    TimeUnit unit() default TimeUnit.MINUTES;

    boolean isRandom() default true;
}

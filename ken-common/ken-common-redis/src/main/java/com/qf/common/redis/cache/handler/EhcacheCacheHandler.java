package com.qf.common.redis.cache.handler;

import com.qf.common.redis.cache.myinter.MemoryCacheHandler;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class EhcacheCacheHandler extends MemoryCacheHandler {

    @Autowired
    private CacheManager cacheManager;

    private Cache cache;

    @PostConstruct
    public void init(){
        cache = cacheManager.getCache("localCache");
    }

    @Override
    public boolean putCache(String key, Serializable value) {
        Element element = new Element(key, value);
        cache.put(element);
        return true;
    }

    /**
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public boolean putCache(String key, Serializable value, long timeout, TimeUnit unit) {
        Element element = new Element(key, value,
                (int)unit.toSeconds(timeout), (int)unit.toSeconds(timeout));
        cache.put(element);
        return false;
    }

    @Override
    public Serializable getCache(String key) {
        Element element = cache.get(key);
        return element != null ? (Serializable)element.getObjectValue() : null;
    }

    @Override
    public Serializable delCache(String key) {
        //获取value
        Serializable value = this.getCache(key);
        //删除value
        cache.remove(key);
        return value;
    }

}

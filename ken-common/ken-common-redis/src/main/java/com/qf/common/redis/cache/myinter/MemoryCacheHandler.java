package com.qf.common.redis.cache.myinter;

import com.qf.common.redis.cache.myinter.base.IBaseCacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.io.Serializable;

/**
 * 本地（内存）缓存的处理器
 */
public abstract class MemoryCacheHandler<T extends Serializable> implements IBaseCacheHandler<T> {

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 同步删除集群中的本地缓存
     * @param key
     * @return
     */
    public boolean syncDelMemoryCache(String key){
        //父类统一实现
        redisTemplate.convertAndSend("cache-sync", key);
        System.out.println("发布删除缓存的消息：" + redisTemplate.hashCode());
        return true;
    }
}

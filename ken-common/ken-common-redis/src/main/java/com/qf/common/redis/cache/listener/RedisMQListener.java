package com.qf.common.redis.cache.listener;

import com.qf.common.redis.cache.myinter.MemoryCacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

import java.io.UnsupportedEncodingException;

/**
 * 消息的接收者
 */
public class RedisMQListener implements MessageListener {

    @Autowired
    private MemoryCacheHandler memoryCacheHandler;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        try {
            String key = new String(message.getBody(), "utf-8");
            System.out.println("接收到消息：" + key);

            //调用本地缓存删除该key
            memoryCacheHandler.delCache(key);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

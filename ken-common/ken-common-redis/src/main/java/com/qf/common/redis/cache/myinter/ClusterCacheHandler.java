package com.qf.common.redis.cache.myinter;


import com.qf.common.redis.cache.myinter.base.IBaseCacheHandler;

import java.io.Serializable;

/**
 * 分布式缓存的处理器
 */
public abstract class ClusterCacheHandler<T extends Serializable> implements IBaseCacheHandler<T> {
}

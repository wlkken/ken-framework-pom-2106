package com.qf.common.redis.config;

import com.qf.common.redis.cache.aop.CacheAop;
import com.qf.common.redis.cache.handler.EhcacheCacheHandler;
import com.qf.common.redis.cache.handler.RedisCacheHandler;
import com.qf.common.redis.cache.listener.RedisMQListener;
import com.qf.common.redis.cache.myinter.ClusterCacheHandler;
import com.qf.common.redis.cache.myinter.MemoryCacheHandler;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class MyRedisAutoConfiguration {
    @Bean
    public CacheManager getCacheManager(){

        CacheConfiguration cacheConfiguration = new CacheConfiguration();
        cacheConfiguration.setName("localCache");
        cacheConfiguration.setTimeToIdleSeconds(300);
        cacheConfiguration.setTimeToLiveSeconds(300);
        cacheConfiguration.setMaxElementsInMemory(100000);//最大的缓存数
//        cacheConfiguration.setMaxEntriesInCache(100000);

        net.sf.ehcache.config.Configuration configuration = new net.sf.ehcache.config.Configuration();
        configuration.addCache(cacheConfiguration);

        CacheManager cacheManager = new CacheManager(configuration);
        return cacheManager;
    }

    /**
     * application.yml - cache.memroy.type=xxx cache.cluster.type=redis
     * @return
     */
    @Bean
    public MemoryCacheHandler getMemoryCacheHandler(){
        return new EhcacheCacheHandler();
    }

    @Bean
    public ClusterCacheHandler getClusterCacheHandler(){
        return new RedisCacheHandler();
    }

    @Bean
    @Primary
    public RedisTemplate getRedisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        //设置key的序列化方式
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        System.out.println("自定义的RedisTemplate对象：" + redisTemplate.hashCode());
        return redisTemplate;
    }

    @Bean
    public MessageListener getMesssageListener(){
        return new RedisMQListener();
    }

    /**
     * 多级缓存的AOP
     * @return
     */
    @Bean
    public CacheAop getCacheAop(){
        return new CacheAop();
    }

    /**
     * 注册消息的监听器
     */
    @Bean
    public RedisMessageListenerContainer messageRegister(RedisConnectionFactory redisConnectionFactory, MessageListener getMesssageListener){
        System.out.println("注册RedisMQ的监听：" + getMesssageListener);
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        redisMessageListenerContainer.addMessageListener(getMesssageListener, new ChannelTopic("cache-sync"));
        redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        return redisMessageListenerContainer;
    }
}

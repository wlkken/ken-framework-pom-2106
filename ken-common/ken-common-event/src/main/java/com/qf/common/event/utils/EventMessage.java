package com.qf.common.event.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * 实际的消息实体
 */
public class EventMessage<T> implements Serializable {

    /**
     * 事件id
     */
    private String msgId;

    /**
     * 事件的发送时间
     */
    private Date sendTime;

    /**
     * 事件的类型（登录、注册.....）
     */
    private String eventType;

    /**
     * 事件的实体
     */
    private T event;

    public EventMessage(String eventType, T event) {
        this.eventType = eventType;
        this.event = event;
        this.msgId = UUID.randomUUID().toString();
        this.sendTime = new Date();
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public T getEvent() {
        return event;
    }

    public void setEvent(T event) {
        this.event = event;
    }
}

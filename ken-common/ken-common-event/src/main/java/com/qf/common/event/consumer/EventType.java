package com.qf.common.event.consumer;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 事件标记注解 - 指定事件处理的类型等相关信息
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component
public @interface EventType {

    /**
     * 指定处理的事件类型
     * @return
     */
    String type();

    /**
     * 是否异步处理事件
     * @return
     */
    boolean isAsync() default false;
}

package com.qf.common.event.config;

import com.qf.common.event.publish.EventSend;
import com.qf.common.event.utils.Contacts;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 发布端的配置
 */
@Configuration
public class PublishConfiguration {

    /**
     * 声明交换机
     */
    @Bean
    public FanoutExchange getFanoutExchange(){
        return new FanoutExchange(Contacts.EXCHANGE_NAME, true, false);
    }

    /**
     * 发布端的调用API对象
     * @return
     */
    @Bean
    public EventSend eventSend(){
        return new EventSend();
    }
}

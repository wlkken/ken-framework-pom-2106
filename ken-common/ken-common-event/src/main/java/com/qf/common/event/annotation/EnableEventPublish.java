package com.qf.common.event.annotation;

import com.qf.common.event.config.PublishConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(PublishConfiguration.class)
public @interface EnableEventPublish {
}

package com.qf.common.event.consumer;

/**
 * 消费端需要实现的接口协议
 * @param <T>
 */
public interface IEventHandler<T> {

    /**
     * 开发者需要执行重写该方法，实现事件的处理逻辑
     * @param event
     */
    void eventHandler(T event);
}

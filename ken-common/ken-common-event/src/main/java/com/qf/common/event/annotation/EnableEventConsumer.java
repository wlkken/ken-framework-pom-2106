package com.qf.common.event.annotation;

import com.qf.common.event.config.ConsumerConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(ConsumerConfiguration.class)
public @interface EnableEventConsumer {
}

package com.qf.common.event.utils;

/**
 * 常量接口
 */
public interface Contacts {

    /**
     * 交换机的名称
     */
    String EXCHANGE_NAME = "event-exchange";
}

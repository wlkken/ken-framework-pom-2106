package com.qf.common.core.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    /**
     * 距离当前第i天的日期
     * @param i
     * @return
     */
    public static Date getNowDate(int i){
        Calendar calendar = Calendar.getInstance();//当前时间
        calendar.add(Calendar.DAY_OF_MONTH, i);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}

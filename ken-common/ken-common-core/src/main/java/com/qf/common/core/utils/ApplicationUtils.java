package com.qf.common.core.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 手动获取Spring容器中的Bean对象
 */
@Component
public class ApplicationUtils {

    /**
     * 注入Spring的Bean容器
     */
    @Autowired
    private ApplicationContext applicationContext;

    private static ApplicationContext staticApplicationContext;

    @PostConstruct
    public void init(){
        staticApplicationContext = applicationContext;
    }

    /**
     * 静态方法
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T getBean(Class<T> cls){
       return staticApplicationContext.getBean(cls);
    }
}

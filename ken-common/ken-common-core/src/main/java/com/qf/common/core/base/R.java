package com.qf.common.core.base;

import com.ken.mybatis.protocol.BaseResult;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 统一返回对象
 */
@Data
@Accessors(chain = true)
public class R<T> extends BaseResult implements Serializable {

    /**
     * 响应码
     */
    private Integer code;

    /**
     * 返回信息
     */
    private String msg;

    /**
     * 返回数据
     */
    private T data;
}

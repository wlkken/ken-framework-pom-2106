package com.qf.common.core.utils;

import com.qf.data.entity.auth.base.BaseUser;

public class UserInfo {

    private static ThreadLocal<BaseUser> sysUserVoThreadLocal = new ThreadLocal<>();

    public static void setUser(BaseUser baseUser){
        UserInfo.sysUserVoThreadLocal.set(baseUser);
    }

    public static <T extends BaseUser> T getUser(){
        return (T) UserInfo.sysUserVoThreadLocal.get();
    }

    public static void clear(){
        UserInfo.sysUserVoThreadLocal.remove();
    }
}

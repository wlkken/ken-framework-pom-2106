package com.qf.common.core.base;

/**
 * 响应枚举
 */
public enum Codes {

    SUCC(200, "成功"),
    SERVER_ERROR(500, "服务器异常，请稍后再试！"),
    PARAM_ERROR(400, "参数校验异常！"),
    AUTH_ERROR(401, "身份验证失败！"),
    TOKEN_ERROR(402, "令牌验证失败！"),
    TOKEN_REFRESH_ERROR(403, "令牌刷新失败！"),
    ACCESS_ERROR(405, "权限不足！"),
    CODE_ERROR(406, "验证码错误");

    public Integer code;
    public String msg;

    Codes(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

package com.qf.common.core.base;

/**
 * 封装R对象的工具类
 */
public class RetUtils {

    public static <T> R create(Integer code, String msg, T data){
        return new R()
                .setCode(code)
                .setMsg(msg)
                .setData(data);
    }

    public static <T> R create(Codes codes, T data){
        return RetUtils.create(codes.code, codes.msg, data);
    }

    public static <T> R createSucc(T data){
        return RetUtils.create(Codes.SUCC.code, Codes.SUCC.msg, data);
    }

    public static <T> R createFail(Integer code, String msg){
        return RetUtils.create(code, msg, null);
    }
}

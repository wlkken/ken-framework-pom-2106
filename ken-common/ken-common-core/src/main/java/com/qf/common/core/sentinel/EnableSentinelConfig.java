package com.qf.common.core.sentinel;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 是否开启Sentinel的配置读取
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(SentinelConfigInit.class)
public @interface EnableSentinelConfig {
}

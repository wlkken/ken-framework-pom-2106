package com.qf.common.security.exception;

import com.qf.common.core.base.Codes;
import com.qf.common.core.base.R;
import com.qf.common.core.base.RetUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class AccessDeniedExceptionHandler {

    @ExceptionHandler(AccessDeniedException.class)
    public R accessHandler(){
        return RetUtils.create(Codes.ACCESS_ERROR, null);
    }
}

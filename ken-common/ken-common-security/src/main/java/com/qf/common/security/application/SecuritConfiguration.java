package com.qf.common.security.application;

import com.qf.common.security.exception.AccessDeniedExceptionHandler;
import com.qf.common.security.interceptor.SecurityInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 微服务权限控制的核心配置类
 */
@Configuration
//权限注解生效@PreAuthorize
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import(AccessDeniedExceptionHandler.class)
public class SecuritConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //放行所有请求，无需认证，认证过程已经在Gateway中实现
        http
                .csrf().disable()
                .authorizeRequests().anyRequest().permitAll();
    }

    /**
     * 权限设置拦截器
     * @return
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE + 100)
    public HandlerInterceptorAdapter getSecurityInterceptor(){
        return new SecurityInterceptor();
    }
}

package com.qf.common.db.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.qf.data")
public class MyDataBaseConfiguration {
}

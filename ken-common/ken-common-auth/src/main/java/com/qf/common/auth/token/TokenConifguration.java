package com.qf.common.auth.token;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * Oauth2的令牌管理
 */
@Configuration
public class TokenConifguration {

    private static final String SINGLE_KEY = "asdfasdfl123123kadsflksahd124fkjhsad";

    @Bean
    public TokenStore getTokenStore(JwtAccessTokenConverter getJwtAccessTokenConverter){
        return new JwtTokenStore(getJwtAccessTokenConverter);
    }

    @Bean
    public MyTokenEnhancer myTokenEnhancer(){
        return new MyTokenEnhancer();
    }

    /**
     * 自定义的令牌转换器
     * @return
     */
    @Bean
    public MyTokenConcerter getMyTokenConcerter(){
        return new MyTokenConcerter();
    }

    /**
     * JWT令牌的转换器
     * 原始的令牌转换成JWT的令牌（包含相关的用户信息、加密方式）
     * @return
     */
    @Bean
    public JwtAccessTokenConverter getJwtAccessTokenConverter(MyTokenConcerter getMyTokenConcerter){
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        //设置生成令牌时的密钥 - 对称加密  非对称加密
        jwtAccessTokenConverter.setSigningKey(SINGLE_KEY);
        //设置自定义的令牌转换器
        jwtAccessTokenConverter.setAccessTokenConverter(getMyTokenConcerter);
        return jwtAccessTokenConverter;
    }
}

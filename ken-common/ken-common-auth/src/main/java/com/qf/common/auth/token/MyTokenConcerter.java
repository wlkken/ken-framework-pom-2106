package com.qf.common.auth.token;

import com.qf.common.core.utils.BaseUserUtils;
import com.qf.data.entity.auth.base.BaseUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;

import java.util.Map;

/**
 * 自定义的令牌转换器 -- 自定义Jwt令牌中用户信息的内容
 */
public class MyTokenConcerter extends DefaultAccessTokenConverter {

    /**
     * 重写构造方法
     */
    public MyTokenConcerter(){
        super.setUserTokenConverter(new UserAuthenticationConverter() {
            /**
             * 将登陆的用户信息 转换成 jwt令牌的内容 -- 认证服务器
             * @param userAuthentication
             * @return
             */
            @Override
            public Map<String, ?> convertUserAuthentication(Authentication userAuthentication) {
                BaseUser baseUser = (BaseUser) userAuthentication.getPrincipal();
                return baseUser.getUserInfoAndFromType2Jwt();
            }

            /**
             * 通过jwt令牌转换成用户登录凭证 -- 资源服务器
             * @param map
             * @return
             */
            @Override
            public Authentication extractAuthentication(Map<String, ?> map) {
                BaseUser baseUser = BaseUserUtils.createUser(map);
                return new UsernamePasswordAuthenticationToken(baseUser, null, baseUser.getAuthorities());
            }
        });
    }
}

package com.qf.common.auth.token;

import com.qf.data.entity.auth.base.BaseUser;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.Map;

/**
 * 令牌增强器
 */
public class MyTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        //获取当前的用户信息
        BaseUser baseUser = (BaseUser) authentication.getPrincipal();
        //获取增强的数据
        Map<String, Object> userInfo2Jwt = (Map<String, Object>) baseUser.getUserInfo2Jwt();

        DefaultOAuth2AccessToken newAccessToken = new DefaultOAuth2AccessToken(accessToken);
        newAccessToken.setAdditionalInformation(userInfo2Jwt);
        return newAccessToken;
    }
}

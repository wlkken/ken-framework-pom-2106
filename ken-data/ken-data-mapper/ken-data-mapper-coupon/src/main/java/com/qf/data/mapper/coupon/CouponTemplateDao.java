package com.qf.data.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.coupon.CouponTemplate;

/**
 * (CouponTemplate)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-01 11:15:07
 */
public interface CouponTemplateDao extends BaseMapper<CouponTemplate> {

}

package com.qf.data.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.coupon.RedEnvelopesDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 红包明细表(RedEnvelopesDetails)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-06 14:50:19
 */
public interface RedEnvelopesDetailsDao extends BaseMapper<RedEnvelopesDetails> {

    List<RedEnvelopesDetails> queryRedEnvelopesDetails(@Param("redId") Long redId, @Param("limit") Integer limit);

    int updateRedDetailShareId(@Param("dIds") List<Long> detilsId,  @Param("sId") Long shareId);

    int insertBatch(@Param("details") List<RedEnvelopesDetails> redEnvelopesDetails);
}

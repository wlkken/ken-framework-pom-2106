package com.qf.data.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.coupon.RedEnvelopes;

/**
 * 红包表(RedEnvelopes)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-06 14:49:55
 */
public interface RedEnvelopesDao extends BaseMapper<RedEnvelopes> {

}

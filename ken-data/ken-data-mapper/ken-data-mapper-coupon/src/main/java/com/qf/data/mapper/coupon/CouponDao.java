package com.qf.data.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.coupon.Coupon;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Coupon)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-01 11:15:36
 */
public interface CouponDao extends BaseMapper<Coupon> {

    List<Coupon> queryByUser(@Param("uid") Long uid, @Param("status") Integer status);
}

package com.qf.data.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.coupon.CouponUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (CouponUser)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-01 11:15:59
 */
public interface CouponUserDao extends BaseMapper<CouponUser> {

    int updateStatus(@Param("cuids") List<Long> cuids, @Param("status") Integer status);

    List<CouponUser> queryCouponUserByUidAndLimit(@Param("uid") Long uid, @Param("limit") Integer limit);

    int insertBatch(@Param("couponUsers") List<CouponUser> couponUsers);
}

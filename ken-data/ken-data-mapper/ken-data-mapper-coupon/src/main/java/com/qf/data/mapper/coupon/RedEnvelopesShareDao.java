package com.qf.data.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.coupon.RedEnvelopesShare;

/**
 * 红包份额表(RedEnvelopesShare)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-06 14:50:46
 */
public interface RedEnvelopesShareDao extends BaseMapper<RedEnvelopesShare> {

}

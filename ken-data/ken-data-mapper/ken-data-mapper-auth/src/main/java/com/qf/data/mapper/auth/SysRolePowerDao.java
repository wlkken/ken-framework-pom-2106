package com.qf.data.mapper.auth;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.auth.SysRolePower;

/**
 * (SysRolePower)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-22 09:31:39
 */
public interface SysRolePowerDao extends BaseMapper<SysRolePower> {

}

package com.qf.data.mapper.auth;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.auth.SysRole;
import com.qf.data.entity.auth.vo.SysRoleVo;

import java.util.List;

/**
 * (SysRole)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-21 14:26:13
 */
public interface SysRoleDao extends BaseMapper<SysRole> {

    List<SysRoleVo> queryRolesByUid(Long uid);

    List<SysRole> queryHasRolesByUid(Long uid);
}

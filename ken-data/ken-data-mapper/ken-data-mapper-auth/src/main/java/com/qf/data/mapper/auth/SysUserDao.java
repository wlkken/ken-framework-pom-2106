package com.qf.data.mapper.auth;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.auth.SysUser;

/**
 * (SysUser)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-21 14:25:46
 */
public interface SysUserDao extends BaseMapper<SysUser> {

}

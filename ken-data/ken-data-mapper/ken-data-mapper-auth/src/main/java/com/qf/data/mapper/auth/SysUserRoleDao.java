package com.qf.data.mapper.auth;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.auth.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (SysUserRole)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-21 14:26:43
 */
public interface SysUserRoleDao extends BaseMapper<SysUserRole> {

    int insertBatch(@Param("urs") List<SysUserRole> userRoles);
}

package com.qf.data.mapper.auth;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.auth.SysPower;
import com.qf.data.entity.auth.dto.SysPowerDto;
import com.qf.data.entity.auth.vo.SysPowerVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (SysPower)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-22 09:29:28
 */
public interface SysPowerDao extends BaseMapper<SysPower> {

    List<SysPowerVo> queryAll();

    List<SysPowerDto> queryPowersByRoid(Long roid);

    List<SysPower> queryHasPowersByRoid(@Param("roids") List<Long> roids);

    List<SysPower> queryHasPowersByUid(Long uid);
}

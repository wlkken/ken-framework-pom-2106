package com.qf.data.mapper.auth;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.auth.WxUser;

/**
 * (WxUser)表数据库访问层
 *
 * @author makejava
 * @since 2021-11-11 09:16:48
 */
public interface WxUserDao extends BaseMapper<WxUser> {

}

package com.qf.data.mapper.hotel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.hotel.CHotel;
import com.qf.data.entity.hotel.vo.CHotelVo;

import java.util.List;

/**
 * (CHotel)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-20 15:27:44
 */
public interface CHotelDao extends BaseMapper<CHotel> {

    List<CHotelVo> queryAllHotelAndCity();
}

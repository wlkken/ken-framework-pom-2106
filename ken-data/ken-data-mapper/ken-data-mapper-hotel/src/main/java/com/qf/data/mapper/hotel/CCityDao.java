package com.qf.data.mapper.hotel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.hotel.CCity;

/**
 * (CCity)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-19 15:56:03
 */
public interface CCityDao extends BaseMapper<CCity> {

}

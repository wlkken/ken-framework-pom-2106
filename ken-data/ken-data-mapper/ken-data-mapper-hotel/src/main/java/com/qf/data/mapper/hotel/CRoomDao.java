package com.qf.data.mapper.hotel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.data.entity.hotel.CRoom;

/**
 * (CRoom)表数据库访问层
 *
 * @author makejava
 * @since 2021-11-13 13:02:37
 */
public interface CRoomDao extends BaseMapper<CRoom> {

}

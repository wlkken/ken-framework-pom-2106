package com.qf.data.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (Coupon)表实体类
 *
 * @author makejava
 * @since 2021-12-01 11:15:35
 */
@Data
@Accessors(chain = true)
public class Coupon extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Long cpId;
    //优惠券标题
    private String subject;
    //优惠券副标题
    private String content;
    //优惠券描述
    private String info;
    //优惠券优惠规则动态字段
    private String ruleInfo;
    //优惠券使用限制的动态字段
    private String limitInfo;
}

package com.qf.data.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * (CouponUser)表实体类
 *
 * @author makejava
 * @since 2021-12-01 11:15:59
 */
@Data
@Accessors(chain = true)
public class CouponUser extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Long cuId;
    //用户id
    private Long uId;
    //优惠券id
    private Long cpId;
    //优惠券领取数量
    private Integer number;
    //优惠券到期时间
    private Date endTime;
}

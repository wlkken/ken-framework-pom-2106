package com.qf.data.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (CouponTemplate)表实体类
 *
 * @author makejava
 * @since 2021-12-01 11:15:06
 */
@Data
@Accessors(chain = true)
public class CouponTemplate extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Long cId;
    //模板名称
    private String templateName;
    //模板类型 0-优惠规则模板 1-使用限制模板
    private Integer type;
    //实现类的全路径限定名
    private String classPath;
    //模板动态字段
    private String dyncTemplate;
}

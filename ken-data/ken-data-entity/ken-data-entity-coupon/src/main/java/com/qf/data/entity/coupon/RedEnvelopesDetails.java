package com.qf.data.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 红包明细表(RedEnvelopesDetails)表实体类
 *
 * @author makejava
 * @since 2021-12-06 14:50:19
 */
@Data
@Accessors(chain = true)
public class RedEnvelopesDetails extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Long id;
    //红包id
    private Long redId;
    //用户领取券id
    private Long cuId;
    //券id
    private Long cpId;
    //红包份额id
    private Long shareId;
}

package com.qf.data.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 红包表(RedEnvelopes)表实体类
 *
 * @author makejava
 * @since 2021-12-06 14:49:54
 */
@Data
@Accessors(chain = true)
public class RedEnvelopes extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Long id;
    //红包发送人的id
    private Long sendUid;
    //0-固定 1-随机
    private Integer type;
    //红包发放券的数量
    private Integer couponNumber;
    //红包领取的人数
    private Integer count;
    //红包的过期时间
    private Date timeOut;
}

package com.qf.data.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 红包份额表(RedEnvelopesShare)表实体类
 *
 * @author makejava
 * @since 2021-12-06 14:50:46
 */
@Data
@Accessors(chain = true)
public class RedEnvelopesShare extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Long id;
    //红包id
    private Long redId;
    //红包领取者的id
    private Long getUid;
    //领券的数量
    private Integer getCouponNumber;
}

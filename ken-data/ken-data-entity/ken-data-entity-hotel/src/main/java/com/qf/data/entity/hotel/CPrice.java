package com.qf.data.entity.hotel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * (CPrice)表实体类
 *
 * @author makejava
 * @since 2021-11-13 13:03:04
 */
@Data
@Accessors(chain = true)
public class CPrice extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long pId;
    private Long rId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pDate;
    private BigDecimal pPrice;
    private Integer pHasNumber;

    //酒店ID
    @TableField(exist = false)
    private Long hid;
}

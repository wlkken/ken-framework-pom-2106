package com.qf.data.entity.hotel.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CHotelVo implements Serializable {

    /**
     * 酒店id
     */
    private Long hid;

    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 行政区域
     */
    private String hotelRegx;

    /**
     * 酒店名称
     */
    private String hotelName;

    /**
     * 酒店图片
     */
    private String hotelImages;

    /**
     * 酒店地址
     */
    private String hotelAddress;

    /**
     * 评分
     */
    private Double hotelStar;
}

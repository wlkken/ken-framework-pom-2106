package com.qf.data.entity.hotel.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CRoomVo implements Serializable {

    private Long rId;
    private String roomName;
    private String roomImages;
    private Integer roomNumber;
    //房间平均价
    private BigDecimal roomAvgPrice;
}

package com.qf.data.entity.hotel.dto;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class CPriceDto implements Serializable {

    @Field(type = FieldType.Integer, index = false)
    private Long pId;

    @Field(type = FieldType.Long)
    private Date pDate;

    @Field(type = FieldType.Double)
    private BigDecimal pPrice;

    @Field(type = FieldType.Integer)
    private Integer pHasNumber;
}

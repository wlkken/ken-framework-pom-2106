package com.qf.data.entity.hotel.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@Document(indexName = "hotel-index", shards = 1, replicas = 0)
public class CHotelDto implements Serializable {

    @Id
    private Long hId;

    @MultiField(
            mainField = @Field(type = FieldType.Text, analyzer = "ik_max_word"),
            otherFields = {
                    @InnerField(type = FieldType.Text, analyzer = "pinyin", suffix = "pinyin")
            }
    )
    private String cityName;

    @MultiField(
            mainField = @Field(type = FieldType.Text, analyzer = "ik_max_word"),
            otherFields = {
                    @InnerField(type = FieldType.Text, analyzer = "pinyin", suffix = "pinyin")
            }
    )
    private String hotelRegx;

    @MultiField(
            mainField = @Field(type = FieldType.Text, analyzer = "ik_max_word"),
            otherFields = {
                    @InnerField(type = FieldType.Text, analyzer = "pinyin", suffix = "pinyin")
            }
    )
    private String hotelName;

    /**
     * 图片不分词不参与搜索
     */
    @Field(type = FieldType.Keyword, index = false)
    private String hotelImages;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String hotelAddress;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String hotelContent;

    @MultiField(
            mainField = @Field(type = FieldType.Text, analyzer = "ik_max_word"),
            otherFields = {
                    @InnerField(type = FieldType.Text, analyzer = "pinyin", suffix = "pinyin")
            }
    )
    private String hotelKeyword;

    /**
     * 经纬度坐标
     */
    @GeoPointField
    private Double[] location = new Double[2];

    /**
     * 酒店客房列表
     */
    @Field(type = FieldType.Nested)
    private List<CRoomDto> rooms = new ArrayList<>();

    /**
     * 酒店的点击量
     */
    @Field(type = FieldType.Integer)
    private Integer djl = 0;

    /**
     * 距离
     */
    @ScriptedField
    private Double distance;

    /**
     * 平均价格
     */
    @ScriptedField
    private Double avgPrice;

}

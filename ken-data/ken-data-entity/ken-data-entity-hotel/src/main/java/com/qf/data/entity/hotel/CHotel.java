package com.qf.data.entity.hotel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (CHotel)表实体类
 *
 * @author makejava
 * @since 2021-10-20 15:27:43
 */
@Data
@Accessors(chain = true)
public class CHotel extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long hId;
    private Long cId;
    private String hotelRegx;
    private String hotelName;
    private String hotelImages;
    private String hotelAddress;
    private String hotelContent;
    private Double hotelStar;
    private Integer hotelLevel;
    private Double hotelLon;
    private Double hotelLat;
    private String hotelKeyword;
}

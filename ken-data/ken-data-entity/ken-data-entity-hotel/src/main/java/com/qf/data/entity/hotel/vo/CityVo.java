package com.qf.data.entity.hotel.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class CityVo implements Serializable {

    private String cityName;
    private String cityPinYin;
    private String cityPY;
}

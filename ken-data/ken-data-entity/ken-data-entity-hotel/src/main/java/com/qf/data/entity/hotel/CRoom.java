package com.qf.data.entity.hotel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * (CRoom)表实体类
 *
 * @author makejava
 * @since 2021-11-13 13:02:35
 */
@Data
@Accessors(chain = true)
public class CRoom extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long rId;
    private Long hId;
    private String roomName;
    private String roomImages;
    private String roomContent;
    private Integer roomNumber;
    private BigDecimal roomDefaultPrice;
}

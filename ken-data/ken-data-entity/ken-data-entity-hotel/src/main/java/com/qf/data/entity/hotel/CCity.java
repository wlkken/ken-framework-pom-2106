package com.qf.data.entity.hotel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (CCity)表实体类
 *
 * @author makejava
 * @since 2021-10-19 15:56:02
 */
@Data
@Accessors(chain = true)
public class CCity extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long cId;
    private String cityName;
    private String cityPinyin;
}

package com.qf.data.entity.hotel.dto;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.InnerField;
import org.springframework.data.elasticsearch.annotations.MultiField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 客房的DTO对象
 */
@Data
public class CRoomDto implements Serializable {

    @Field(type = FieldType.Integer, index = false)
    private Long rId;

    @MultiField(
            mainField = @Field(type = FieldType.Text, analyzer = "ik_max_word"),
            otherFields = {
                    @InnerField(type = FieldType.Text, analyzer = "pinyin", suffix = "pinyin")
            }
    )
    private String roomName;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String roomContent;

    @Field(type = FieldType.Nested)
    private List<CPriceDto> prices = new ArrayList<>();
}

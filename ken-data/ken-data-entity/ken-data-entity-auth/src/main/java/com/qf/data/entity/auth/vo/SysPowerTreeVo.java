package com.qf.data.entity.auth.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 带有树形结构的权限展示对象
 */
@Data
@Accessors(chain = true)
public class SysPowerTreeVo implements Serializable {

    private Long id;
    private String prName;
    private Integer status;
    private boolean checked;
    private List<SysPowerTreeVo> children;
}

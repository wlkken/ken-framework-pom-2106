package com.qf.data.entity.auth.dto;

import com.qf.data.entity.auth.SysPower;
import lombok.Data;

@Data
public class SysPowerDto extends SysPower {

    private boolean checked;
}

package com.qf.data.entity.auth.vo;

import com.qf.data.entity.auth.WxUser;
import com.qf.data.entity.auth.base.BaseUser;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 代表微信用户的UserDetails实现类
 */
@Data
public class WxUserVo extends WxUser implements BaseUser {

    @Override
    public Map<String, ?> getUserInfo2Jwt() {
        Long wuid = this.getWuId();
        String nickname = this.getNickName();
        String username = this.getUsername();
        String headerImg = this.getHeaderImg();

        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("wuid", wuid);
        userInfo.put("nickname", nickname);
        userInfo.put("username", username);
        userInfo.put("headerImg", headerImg);

        return userInfo;
    }

    @Override
    public int getFromType() {
        return 2;
    }
}

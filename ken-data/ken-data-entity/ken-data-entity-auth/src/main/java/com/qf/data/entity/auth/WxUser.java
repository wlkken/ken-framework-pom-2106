package com.qf.data.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (WxUser)表实体类
 *
 * @author makejava
 * @since 2021-11-11 09:16:47
 */
@Data
@Accessors(chain = true)
public class WxUser extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long wuId;
    //用户名
    private String username;
    //密码
    private String password;
    //昵称
    private String nickName;
    //邮箱地址
    private String email;
    //头像
    private String headerImg;
}

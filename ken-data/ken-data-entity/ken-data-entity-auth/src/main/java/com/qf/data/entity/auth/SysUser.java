package com.qf.data.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (SysUser)表实体类
 *
 * @author makejava
 * @since 2021-10-21 14:25:45
 */
@Data
@Accessors(chain = true)
public class SysUser extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long uId;
    private String username;
    private String password;
    private String nickName;
}

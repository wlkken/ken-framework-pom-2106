package com.qf.data.entity.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * (SysUserRole)表实体类
 *
 * @author makejava
 * @since 2021-10-21 14:26:43
 */
@Data
@Accessors(chain = true)
public class SysUserRole implements Serializable {

    private Long uId;
    private Long roId;
    private Date createTime;
}

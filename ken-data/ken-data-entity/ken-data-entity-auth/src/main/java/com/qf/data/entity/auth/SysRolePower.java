package com.qf.data.entity.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * (SysRolePower)表实体类
 *
 * @author makejava
 * @since 2021-10-22 09:31:39
 */
@Data
@Accessors(chain = true)
public class SysRolePower implements Serializable{

    private Long roId;
    private Long poId;
    private Date createTime;
}

package com.qf.data.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (SysPower)表实体类
 *
 * @author makejava
 * @since 2021-10-22 09:29:26
 */
@Data
@Accessors(chain = true)
public class SysPower extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long prId;
    private Long prPid;
    private String prName;
    private String prFlag;
}

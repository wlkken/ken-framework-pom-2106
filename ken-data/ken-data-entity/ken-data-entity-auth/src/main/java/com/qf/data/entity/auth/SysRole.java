package com.qf.data.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (SysRole)表实体类
 * @author makejava
 * @since 2021-10-21 14:26:13
 */
@Data
@Accessors(chain = true)
public class SysRole extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long roId;
    private String roleName;
    private String roleFlag;
}

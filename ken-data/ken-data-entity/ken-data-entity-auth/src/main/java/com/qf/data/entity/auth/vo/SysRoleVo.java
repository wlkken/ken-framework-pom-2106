package com.qf.data.entity.auth.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (SysRole)表实体类
 *
 * @author makejava
 * @since 2021-10-21 14:26:13
 */
@Data
@Accessors(chain = true)
public class SysRoleVo implements Serializable {

    private Long id;
    private String roleName;
    private String roleFlag;
    private boolean checked;
}

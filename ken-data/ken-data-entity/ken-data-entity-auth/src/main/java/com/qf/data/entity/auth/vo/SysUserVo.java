package com.qf.data.entity.auth.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.qf.data.entity.auth.SysUser;
import com.qf.data.entity.auth.base.BaseUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 自定义Jackson的反序列化
 */
@Data
public class SysUserVo extends SysUser implements BaseUser {

    /**
     * 当前用户拥有的权限列表
     */
    @JsonDeserialize(contentUsing = GrantedAuthorityDeserialize.class)
    private List<GrantedAuthority> authorities;

    @Override
    public Map<String, ?> getUserInfo2Jwt() {
        Long uid = this.getUId();
        String nickname = this.getNickName();
        String username = this.getUsername();

        //权限信息
        Set<String> authoritys = this.getAuthorities()
                .stream()
                .map(grantedAuthority -> grantedAuthority.getAuthority())
                .collect(Collectors.toSet());

        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("uid", uid);
        userInfo.put("nickname", nickname);
        userInfo.put("username", username);
        userInfo.put("authorities", authoritys);

        return userInfo;
    }

    @Override
    public int getFromType() {
        return 1;
    }


    /**
     * 返回权限列表
     *
     * @return
     */
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public void setAuthorities(Collection<? extends GrantedAuthority> collections) {
        this.authorities = (List<GrantedAuthority>) collections;
    }


    @Override
    public String toString() {
        return "SysUserVo{" +
                super.toString() + ",authorities=" + authorities +
                '}';
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    @JsonIgnore
    public Date getUpdateTime() {
        return super.getUpdateTime();
    }

    @Override
    @JsonIgnore
    public Date getCreateTime() {
        return super.getCreateTime();
    }

    @Override
    @JsonIgnore
    public Integer getStatus() {
        return super.getStatus();
    }

    @Override
    @JsonIgnore
    public Integer getDelFlag() {
        return super.getDelFlag();
    }
}

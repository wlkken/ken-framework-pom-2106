package com.qf.data.entity.auth.vo;

import com.qf.data.basic.entity.BaseEntity;
import lombok.Data;

@Data
public class SysPowerVo extends BaseEntity {

    private Long prId;
    private String prPname;
    private String prName;
    private String prFlag;
    private String statusName;
}
